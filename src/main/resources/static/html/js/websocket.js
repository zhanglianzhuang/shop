$(function () {
    reading();
    $("#tArea").keydown(function (event) {
        event = document.all ? window.event : event;
        if ((event.keyCode || event.which) === 13) {
            event.preventDefault();
            $("#send").click();
        }
    });
});

let socket;
let URL = "ws://121.36.101.188:8080/getChat/";
let uid;
let read = false;
if (localStorage.getItem("user")) {
    uid = JSON.parse(localStorage.getItem("user")).id;
} else if (sessionStorage.getItem("user")) {
    uid = JSON.parse(sessionStorage.getItem("user")).id;
} else {
}
function connect(id, name) {
    $("#status").html("<span>连接中.....</span>");
    if (window.WebSocket) {
        socket = new WebSocket(URL + uid);

        socket.onmessage = function (event) {
            let data = JSON.parse(event.data);
            let msg = "<li><div class='chatLeft'>" + data.content + "</div></li>";
            addMsgContent(msg);
        };
        socket.onopen = function () {
            $("#status").html("<span style='background-color: #44b549'>" + name + "</span>");
            // $("#msgContent").html("<span onclick='getMore(" + id + ")'>获取历史消息</span>");
            $(".newList").html("<span onclick='getMore(" + id + ")'>获取历史消息</span>");

            if (read) {
                getMore(id);
            }

            $("#m" + id + "").html("<span class=\"profile__status profile__status--confirmed\">已处理</span>");
        };
        socket.onclose = function () {
            $("#status").html("<span style='background-color: red'>聊天服务已断开连接</span>");
            setTimeout("connect(id,name)", 3000);
        };
        createSend(id);

    } else {
        $("#status").html("<span style='background-color: red'>该浏览器不支持WebSocket协议！</span>");
    }


}

function addMsgContent(msg) {
    let msgBox = document.getElementById("msgContent");
    // $("#msgContent").append(msg);
    $(".newList").append(msg);
    msgBox.scrollTop = msgBox.scrollHeight;

}

function createSend(id) {
    let str = "<textarea id=\"tArea\" style=\"width: 100%;height: 60px; border: none;outline: none;resize: none;\" name=\"\" rows=\"\" cols=\"\"></textarea>" +
        "<input class=\"send\" id='send' type=\"button\" name=\"msgBtn\" value=\"发送\" onclick=\"send(" + id + ")\" style=\" color: #ffffff;font-size: 12px;\"/>";
    $("#sendBox").html(str);
}

function send(id) {
    let message = $("#tArea").val();
    if (!window.WebSocket) return;
    if (message === "") {
        alert("不能发送空消息！");
        return;
    }
    if (socket.readyState === WebSocket.OPEN) {
        let msg = "<li><div class='chatRight'>" + message + "</div></li>";
        this.addMsgContent(msg);
        let date = new Date(+new Date() + 8 * 3600 * 1000).toISOString().replace(/T/g, 'T').replace(/\.[\d]{3}Z/, '');

        let data = {
            fromUid: uid,
            toUid: id,
            content: message,
            CDATE: date,
            reading: false
        };


        socket.send(JSON.stringify(data));
        // $("input[name=message]").val("");
        $("#tArea").val("");
    } else {
        alert("无法建立WebSocket连接！");
    }
}

function close() {
    socket.close();
    reading();
}

function reading() {
    let uid;
    if (localStorage.getItem("user")) {
        uid = JSON.parse(localStorage.getItem("user")).id;
    } else if (sessionStorage.getItem("user")) {
        uid = JSON.parse(sessionStorage.getItem("user")).id;
    } else {
    }
    if (uid) {
        $.ajax({
            type: "GET",
            url: "http://121.36.101.188:8080/api/v1/message/getUnReading?uid=" + uid,
            async: false,
            success: function (result) {
                if (result.code === 1) {

                    if (result.data) {
                        tips();
                        read = true;
                    } else {
                        read = false;
                    }
                } else {

                }
            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });

    }
}


function getMore(id) {
    // $("#msgContent").html("");
    $(".newList").html("");
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/message/getMessage?uid=" + uid,
        async: false,
        success: function (result) {
            if (result.code === 1) {
                if (result.data.length) {
                    for (let i = 0; i < result.data.length; i++) {
                        let msg;
                        if (result.data[i].fromUid === uid && result.data[i].toUid === id) {
                            msg = "<li><div class='chatRight'>" + result.data[i].content + "</div></li>"
                        }
                        if (result.data[i].fromUid === id && result.data[i].toUid === uid) {
                            msg = "<li><div class='chatLeft'>" + result.data[i].content + "</div></li>"
                        }
                        addMsgContent(msg);
                        flag = 3;
                        $(".contact").css({"background-image": "url(\"img/comments.png\")"});
                    }
                }
            }
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });

}

let flag = 1;

function tips() {
    if (flag === 1) {
        $(".contact").css({"background-image": "url(\"img/comments-h.png\")"});
        flag = 2;
    } else if (flag === 2) {
        $(".contact").css({"background-image": "url(\"img/comments.png\")"});
        flag = 1;
    }
    window.setTimeout("tips()", 1000);
}