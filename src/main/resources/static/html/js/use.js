$(function () {
    $("#searchI").click(
        function () {
            window.location.href = "search.html?Info=" + $(".header__input").val();
        }
    );
    $('body').bind('keypress', function (event) {
        if (event.keyCode === "13") {
            event.preventDefault();
            //回车执行查询
            $('.sign__btn').click();
        }
    });
});
function userCheck() {
    let username = document.getElementById("user").value;
    let nameCheck = document.getElementById("nameCheck");
    if (!(/^(1[3-9]+\d{9})$/.test(username))) {
        nameCheck.innerHTML = "手机号错误";
        return false;
    }
    nameCheck.innerHTML = "";
    return true;
}

function pwdCheck() {
    let pwd1 = document.getElementById("pwd1").value;
    let pwd2 = document.getElementById("pwd2").value;
    let pwd1check = document.getElementById("pwd1check");
    let pwd2check = document.getElementById("pwd2check");
    if (pwd1 === "") {
        pwd1check.innerHTML = "请设置密码";
        return false;
    } else if (pwd1.length < 6 || pwd1.length > 18) {
        pwd1check.innerHTML = "6-18个字符";
        return false;
    } else {
        pwd1check.innerHTML = "";
    }
    if (pwd2 !== pwd1) {
        pwd2check.innerHTML = "密码不一致";
        return false;
    }
    pwd2check.innerHTML = "";
    return true;
}

function register() {
    let json = {
        "phoneNumber": $("#user").val(),
        "password": $("#pwd1").val(),
        // "basePic": basePic
    };
    $.ajax({
        type: "POST",
        data: JSON.stringify(json),
        url: "http://121.36.101.188:8080/api/v1/user/register",
        async: false,
        contentType: 'application/json',
        dateType: "json",
        success: function (result) {
            if (result) {
                alert("注册成功！请登录");
                window.location.href = "signin.html";
            } else alert("注册失败!");
        },
        error: function (errorMsg) {
            alert("注册失败");
        }
    });
}

function login() {
    let json = {
        "phoneNumber": $("#user").val(),
        "password": $("#pwd1").val()
    };
    console.log(json);
    $.ajax({
        type: "POST",
        data: JSON.stringify(json),
        url: "http://121.36.101.188:8080/api/v1/user/login",
        async: false,
        contentType: 'application/json',
        dateType: "json",
        success: function (result) {
            console.log(result);
            if (result.code === 1) {
                getInfo(result.data.id);
            } else alert("登录失败!");
        },
        error: function (errorMsg) {
            alert("登录失败");
        }
    });
}

function getInfo(uid) {
    $.ajax({
        type: "POST",
        url: "http://121.36.101.188:8080/api/v1/user/info?uid=" + uid,
        async: false,
        success: function (result) {
            if (result.code === 1 && !result.data.logout) {
                if ($("#remember").prop("checked")) {
                    localStorage.setItem("user", JSON.stringify(result.data));
                } else {
                    sessionStorage.setItem("user", JSON.stringify(result.data));
                }
                if (result.data.roleId === 4) {
                    window.location.href = "admin.html";
                } else {
                    window.location.href = "index.html";
                }
            } else alert("登录失败!");
        },
        error: function (errorMsg) {
            alert("登录失败");
        }
    });
}