$(function () {
    checkLogin();
    getAdmin();
    $("#beCreator").click(
        function () {
            let account = $("#account").val();
            if ($("#remember").prop("checked") && account) {
                changeRole(checkUser(), 2, account);
            } else {
                alert("请确认信息！");
            }
        }
    );
    $("#searchI").click(
        function () {
            window.location.href = "search.html?Info=" + $(".header__input").val();
        }
    );

    $("#search").keydown(function (event) {
        event = document.all ? window.event : event;
        if ((event.keyCode || event.which) === 13) {
            $("#searchI").click();
        }
    });

    $(".vip-h").hover(function () {
        $(".vip-tips").show();
    }, function () {
        $(".vip-tips").hide();
    });

    $(".service").hover(function () {
        $(".admin-list").show();
    }, function () {
        $(".admin-list").hide();
    });
});
function quit() {
    localStorage.clear();
    sessionStorage.clear();
    window.location.href = "index.html";
}
function checkLogin() {
    if (localStorage.getItem("user")) {
        let lSuccess = '<a href="profile.html" class="header__noLogin">' + JSON.parse(localStorage.getItem("user")).username + '</a><a href="javascript:;" class="header__noLogin" onclick="quit()">退出</a>';
        $("#userMenu").html(lSuccess); //内容
        checkPower();
    } else if (sessionStorage.getItem("user")) {
        let lSuccess = '<a href="profile.html" class="header__noLogin">' + JSON.parse(sessionStorage.getItem("user")).username + '</a><a href="javascript:;" class="header__noLogin" onclick="quit()">退出</a>';
        $("#userMenu").html(lSuccess); //内容
        checkPower();
    } else {
        let lError = ' <a href="signin.html" class="header__login"><span>登录</span></a>';
        $("#userMenu").html(lError); //内容
        checkPower();
    }
}

function checkUser() {
    if (localStorage.getItem("user")) {
        return JSON.parse(localStorage.getItem("user")).id;
    } else if (sessionStorage.getItem("user")) {
        return JSON.parse(sessionStorage.getItem("user")).id;
    } else {
        window.location.href = "signin.html";
        return false;
    }
}

function checkFavors() {
    let uid;
    if (localStorage.getItem("user")) {
        uid = JSON.parse(localStorage.getItem("user")).id;
    } else if (sessionStorage.getItem("user")) {
        uid = JSON.parse(sessionStorage.getItem("user")).id;
    } else {
    }
    if (uid) {
        $.ajax({
            type: "GET",
            url: "http://121.36.101.188:8080/api/v1/favor/getFavors?uid=" + uid,
            async: false,
            success: function (result) {
                if (result.code === 1) {
                    if (result.data.length) {
                        for (let i = 0; i < result.data.length; i++) {
                            let str = "<button class=\"card__favorite\" type=\"button\" onclick='reFavors(" + result.data[i].id + ")'>\n" +
                                "                        <img src=\"img/favorites-fill.png\">\n" +
                                "                    </button>";
                            $("." + result.data[i].id + "").html(str);
                        }
                    }
                }
            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });
    }
}

function checkCars() {
    let uid;
    if (localStorage.getItem("user")) {
        uid = JSON.parse(localStorage.getItem("user")).id;
    } else if (sessionStorage.getItem("user")) {
        uid = JSON.parse(sessionStorage.getItem("user")).id;
    } else {
    }
    if (uid) {
        $.ajax({
            type: "GET",
            url: "http://121.36.101.188:8080/api/v1/car/getCars?uid=" + uid,
            async: false,
            success: function (result) {
                if (result.code === 1) {
                    if (result.data.length) {
                        for (let i = 0; i < result.data.length; i++) {
                            // let str = "<button class=\"inCar\" type=\"button\" '>已添加</button>";
                            let str = "<button class=\"card__favorite inCar\" type=\"button\">\n" +
                                "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                                "                                             viewBox='0 0 512 512'>\n" +
                                "                                            <line x1='256' y1='112' x2='256' y2='400'\n" +
                                "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                                "                                            <line x1='400' y1='256' x2='112' y2='256'\n" +
                                "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                                "                                        </svg>\n" +
                                "                                    </button>";

                            $(".c" + result.data[i].id + "").html(str);
                        }
                    }
                }
            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });
    }
}

function checkBuying() {
    let uid;
    if (localStorage.getItem("user")) {
        uid = JSON.parse(localStorage.getItem("user")).id;
    } else if (sessionStorage.getItem("user")) {
        uid = JSON.parse(sessionStorage.getItem("user")).id;
    } else {
    }
    if (uid) {
        $.ajax({
            type: "GET",
            url: "http://121.36.101.188:8080/api/v1/user/getBuying?uid=" + uid,
            async: false,
            success: function (result) {
                if (result.code === 1) {
                    if (result.data.length) {
                        for (let i = 0; i < result.data.length; i++) {
                            let str = "<button type=\"button\" class=\"inCar\" >已购买</button>";
                            $(".b" + result.data[i].id + "").html(str);
                            let btn = "<button class=\"card__favorite inCar\" type=\"button\">\n" +
                                "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                                "                                             viewBox='0 0 512 512'>\n" +
                                "                                            <line x1='256' y1='112' x2='256' y2='400'\n" +
                                "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                                "                                            <line x1='400' y1='256' x2='112' y2='256'\n" +
                                "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                                "                                        </svg>\n" +
                                "                                    </button>";

                            $(".c" + result.data[i].id + "").html(btn);
                        }
                    }
                }
            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });
    }
}

function addCar(cid) {
    let uid = checkUser();
    if (uid) {
        let json = {
            "uid": uid,
            "cid": cid
        };
        if (confirm("添加到购物车？")) {
            $.ajax({
                type: "POST",
                data: JSON.stringify(json),
                url: "http://121.36.101.188:8080/api/v1/car/add",
                async: false,
                contentType: 'application/json',
                dateType: "json",
                success: function (result) {
                    if (result.code === 1) {
                        checkCars();
                    } else alert("添加失败!");

                },
                error: function (errorMsg) {
                    alert("添加失败");
                }
            });
        }
    }
}

function addFavors(cid) {
    let uid = checkUser();
    if (uid) {
        let json = {
            "uid": uid,
            "cid": cid
        };
        $.ajax({
            type: "POST",
            data: JSON.stringify(json),
            url: "http://121.36.101.188:8080/api/v1/favor/add",
            async: false,
            contentType: 'application/json',
            dateType: "json",
            success: function (result) {
                if (result.code === 1) {
                    checkFavors();
                } else alert("收藏失败!");

            },
            error: function (errorMsg) {
                alert("收藏失败");
            }
        });
    }
}

function commodity() {
    let pageNumber = 1;
    let pageSize = 20;
    let statusId = 2;
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/shop/getAllCommodity?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&statusId=" + statusId,
        async: false,
        success: function (result) {
            if (result.code === 1) {
                let classLength = result.data.length;
                $(".comTitle").each(function (i, n) {
                    i = i % classLength;
                    let str = "<a href=\"details.html?cid=" + result.data[i].id + "\">" + result.data[i].commodityName + '</a>';
                    $(this).html(str);
                });
                $(".comPrice").each(function (i, n) {
                    i = i % classLength;
                    let str = '￥' + result.data[i].price;
                    $(this).html(str);
                });
                $(".card__actions").each(function (i, n) {
                    i = i % classLength;
                    let str = "<div class=\"myBuy card__buy b" + result.data[i].id + "\">\n" +
                        "                                        <button type=\"button\" onclick=\"if(checkUser()){VIPBuy(" + result.data[i].id + ")}\" >购买</button>\n" +
                        "                                        </div>" +
                        "<div class=\"myCar c" + result.data[i].id + "\">\n" +
                        "<button class=\"card__favorite\" type=\"button\" onclick='addCar(" + result.data[i].id + ")'>\n" +
                        "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                        "                                             viewBox='0 0 512 512'>\n" +
                        "                                            <line x1='256' y1='112' x2='256' y2='400'\n" +
                        "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                        "                                            <line x1='400' y1='256' x2='112' y2='256'\n" +
                        "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                        "                                        </svg>\n" +
                        "                                    </button>" +
                        "</div>\n" +
                        "                                        <div class=\"myFav " + result.data[i].id + "\">\n" +
                        "                                        <button class=\"card__favorite \" type=\"button\" onclick='addFavors(" + result.data[i].id + ")'>\n" +
                        "                                            <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"512\" height=\"512\" viewBox=\"0 0 512 512\">\n" +
                        "                                                <path d=\"M352.92,80C288,80,256,144,256,144s-32-64-96.92-64C106.32,80,64.54,124.14,64,176.81c-1.1,109.33,86.73,187.08,183,252.42a16,16,0,0,0,18,0c96.26-65.34,184.09-143.09,183-252.42C447.46,124.14,405.68,80,352.92,80Z\" style=\"fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px\"></path>\n" +
                        "                                            </svg>\n" +
                        "                                        </button>\n" +
                        "                                        </div>";
                    $(this).html(str);
                });
                $(".comImg").each(function (i, n) {
                    i = i % classLength;
                    let str = "<a href=\"details.html?cid=" + result.data[i].id + "\">\n" +
                        "                                        <img src=\"data:image/png;base64," + result.data[i].pic + "\" alt=\"\">\n" +
                        // "                                        <img src=\"img/cards/100.jpg\" alt=\"\">\n" +
                        "                                    </a>";
                    $(this).html(str);
                });
            } else alert("获取失败!");
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
    checkFavors();
    checkCars();
    checkBuying();
}

function getFavors() {
    let uid = checkUser();
    if (uid) {
        $.ajax({
            type: "GET",
            url: "http://121.36.101.188:8080/api/v1/favor/getFavors?uid=" + uid,
            async: false,
            success: function (result) {
                let str = '<h2 class="section__title">空空如也</h2>';
                if (result.code === 1) {
                    if (result.data.length) {
                        str = '';
                        for (let i = 0; i < result.data.length; i++) {
                            str += "<div class=\"col-12 col-sm-6 col-xl-4\">\n" +
                                "\t\t\t\t\t\t\t<div class=\"card\">\n" +
                                "\t\t\t\t\t\t\t\t<a href=\"details.html?cid=" + result.data[i].id + "\" class=\"card__cover\">\n" +
                                "\t\t\t\t\t\t\t\t\t<img src=\"data:image/png;base64," + result.data[i].pic + "\" alt=\"\">\n" +
                                "\t\t\t\t\t\t\t\t</a>\n" +
                                "\n" +
                                "\t\t\t\t\t\t\t\t<div class=\"card__title\">\n" +
                                "\t\t\t\t\t\t\t\t\t<h3><a href=\"details.html?cid=" + result.data[i].id + "\" >" + result.data[i].commodityName + "</a></h3>\n" +
                                "\t\t\t\t\t\t\t\t\t<span>￥" + result.data[i].price + "</span>\n" +
                                "\t\t\t\t\t\t\t\t</div>\n" +
                                "\n" +
                                "\t\t\t\t\t\t\t\t<div class=\"card__actions\">\n" +
                                // "<div class=\"myCar card__buy c" + result.data[i].id + "\">\n" +
                                // "                                        <button  type=\"button\" onclick='addCar(" + result.data[i].id + ")'>购买</button>\n" +
                                // "                                        </div>" +
                                // "\n" +


                                "<div class=\"myBuy card__buy b" + result.data[i].id + "\">\n" +
                                "                                        <button type=\"button\" onclick=\"if(checkUser()){VIPBuy(" + result.data[i].id + ")}\" >购买</button>\n" +
                                "                                        </div>" +
                                "<div class=\"myCar c" + result.data[i].id + "\">\n" +
                                "<button class=\"card__favorite\" type=\"button\" onclick='addCar(" + result.data[i].id + ")'>\n" +
                                "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                                "                                             viewBox='0 0 512 512'>\n" +
                                "                                            <line x1='256' y1='112' x2='256' y2='400'\n" +
                                "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                                "                                            <line x1='400' y1='256' x2='112' y2='256'\n" +
                                "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                                "                                        </svg>\n" +
                                "                                    </button>" +
                                "</div>\n" +


                                "\t\t\t\t\t\t\t\t\t<button class=\"card__favorite card__favorite--delete\" type=\"button\" onclick='deleteFavors(" + result.data[i].id + ")'>\n" +
                                "\t\t\t\t\t\t\t\t\t\t<svg xmlns='http://www.w3.org/2000/svg' width='512' height='512' viewBox='0 0 512 512'><path d='M112,112l20,320c.95,18.49,14.4,32,32,32H348c17.67,0,30.87-13.51,32-32l20-320' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='80' y1='112' x2='432' y2='112' style='stroke-linecap:round;stroke-miterlimit:10;stroke-width:32px'/><path d='M192,112V72h0a23.93,23.93,0,0,1,24-24h80a23.93,23.93,0,0,1,24,24h0v40' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='256' y1='176' x2='256' y2='400' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='184' y1='176' x2='192' y2='400' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='328' y1='176' x2='320' y2='400' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/></svg>\n" +
                                "\t\t\t\t\t\t\t\t\t</button>\n" +
                                "\t\t\t\t\t\t\t\t</div>\n" +
                                "\t\t\t\t\t\t\t</div>\n" +
                                "\t\t\t\t\t\t</div>";
                        }
                    }
                    $("#myFavors").html(str);
                }
            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });
        checkCars();
        checkBuying();
    }
}

function getCar() {
    let uid = checkUser();
    let name;
    let phone;
    if (localStorage.getItem("user")) {
        name = JSON.parse(localStorage.getItem("user")).username;
        phone = JSON.parse(localStorage.getItem("user")).phoneNo;
    } else if (sessionStorage.getItem("user")) {
        name = JSON.parse(sessionStorage.getItem("user")).username;
        phone = JSON.parse(sessionStorage.getItem("user")).phoneNo;
    } else {
    }
    $(".name").val(name);
    $(".phone").val(phone);
    if (uid) {
        $.ajax({
            type: "GET",
            url: "http://121.36.101.188:8080/api/v1/car/getCars?uid=" + uid,
            async: false,
            success: function (result) {
                let str = '';
                let info = '';
                let total = 0;
                if (result.code === 1) {
                    if (result.data.length) {
                        for (let i = 0; i < result.data.length; i++) {
                            str += "<tr>\n" +
                                "\t\t\t\t\t\t\t\t\t\t<td>\n" +
                                "\t\t\t\t\t\t\t\t\t\t\t<div class=\"cart__img\">\n" +
                                "\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"data:image/png;base64," + result.data[i].pic + "\" alt=\"\">\n" +
                                "\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
                                "\t\t\t\t\t\t\t\t\t\t</td>\n" +
                                "\t\t\t\t\t\t\t\t\t\t<td>" + result.data[i].commodityName + "</td>\n" +
                                "\t\t\t\t\t\t\t\t\t\t<td>" + result.data[i].creator + "</td>\n" +
                                "\t\t\t\t\t\t\t\t\t\t<td><span class=\"cart__price\">￥" + result.data[i].price + "</span></td>\n" +
                                "\t\t\t\t\t\t\t\t\t\t<td><button class=\"cart__delete\" type=\"button\" onclick='removeCars(" + result.data[i].id + ")'><svg xmlns='http://www.w3.org/2000/svg' width='512' height='512' viewBox='0 0 512 512'><line x1='368' y1='368' x2='144' y2='144' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='368' y1='144' x2='144' y2='368' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/></svg></button></td>\n" +
                                "\t\t\t\t\t\t\t\t\t</tr>";
                            total += result.data[i].price;
                            info += "" + result.data[i].id + ",";
                        }
                    }
                    info = info.substr(0, info.length - 1);
                    $("#myCar").html(str);
                    $(".info").val(info);
                }
                total = total.toFixed(2);
                $("#total").html("￥" + total);
            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });

    }

}

function getCatalog(page) {
    let maxPage = parseInt($("#pageSize").html());
    let pag = "";
    if (page - 1 > 0) {
        pag = pag + "<li class=\"paginator__item paginator__item--prev\">\n" +
            "                                    <a href=\"#\" onclick='getCatalog(" + (page - 1) + ")'>\n" +
            "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
            "                                             viewBox='0 0 512 512'>\n" +
            "                                            <polyline points='244 400 100 256 244 112'\n" +
            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                            <line x1='120' y1='256' x2='412' y2='256'\n" +
            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                        </svg>\n" +
            "                                    </a>\n" +
            "                                </li>\n"
    }
    if (page >= 1 && page <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item paginator__item--active\"><a href=\"#\">" + page + "</a></li>\n"
    }
    if (page + 1 >= 1 && page + 1 <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item\"><a href=\"#\" onclick='getCatalog(" + (page + 1) + ")'>" + (page + 1) + "</a></li>\n"
    }
    if (page + 2 >= 1 && page + 2 <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item\"><a href=\"#\" onclick='getCatalog(" + (page + 2) + ")'>" + (page + 2) + "</a></li>\n"
    }
    if (page + 1 <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item paginator__item--next\">\n" +
            "                                    <a href=\"#\" onclick='getCatalog(" + (page + 1) + ")'>\n" +
            "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
            "                                             viewBox='0 0 512 512'>\n" +
            "                                            <polyline points='268 112 412 256 268 400'\n" +
            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                            <line x1='392' y1='256' x2='100' y2='256'\n" +
            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                        </svg>\n" +
            "                                    </a>\n" +
            "                                </li>";
    }

    $("#myPage").html(pag);

    let pageNumber = page;
    let pageSize = 20;
    let statusId = 2;
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/shop/getAllCommodity?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&statusId=" + statusId,
        async: false,
        success: function (result) {
            let str = '<h2 class="section__title">空空如也</h2>';
            if (result.code === 1) {
                if (result.data.length) {
                    str = '';
                    for (let i = 0; i < result.data.length; i++) {
                        str += "<div class=\"col-12 col-sm-6 col-xl-4 \">\n" +
                            "\t\t\t\t\t\t\t<div class=\"card\">\n" +
                            "\t\t\t\t\t\t\t\t<a href=\"details.html?cid=" + result.data[i].id + "\" class=\"card__cover\">\n" +
                            "\t\t\t\t\t\t\t\t\t<img src=\"data:image/png;base64," + result.data[i].pic + "\" alt=\"\">\n" +
                            "\t\t\t\t\t\t\t\t</a>\n" +
                            "\n" +
                            "\t\t\t\t\t\t\t\t<div class=\"card__title\">\n" +
                            "\t\t\t\t\t\t\t\t\t<h3><a href=\"details.html?cid=" + result.data[i].id + "\">" + result.data[i].commodityName + "</a></h3>\n" +
                            "\t\t\t\t\t\t\t\t\t<span>￥" + result.data[i].price + "</span>\n" +
                            "\t\t\t\t\t\t\t\t</div>\n" +
                            "\n" +
                            "\t\t\t\t\t\t\t\t<div class=\"card__actions\">\n" +
                            // "<div class=\"myCar card__buy c" + result.data[i].id + "\">\n" +
                            // "                                        <button  type=\"button\" onclick='addCar(" + result.data[i].id + ")'>购买</button>\n" +
                            // "                                        </div>" +


                            "    <div class=\"myBuy card__buy b" + result.data[i].id + "\">\n" +
                            "                                        <button type=\"button\" onclick=\"if(checkUser()){VIPBuy(" + result.data[i].id + ")}\" >购买</button>\n" +
                            "                                        </div>" +
                            "<div class=\"myCar c" + result.data[i].id + "\">\n" +
                            "<button class=\"card__favorite\" type=\"button\" onclick='addCar(" + result.data[i].id + ")'>\n" +
                            "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                            "                                             viewBox='0 0 512 512'>\n" +
                            "                                            <line x1='256' y1='112' x2='256' y2='400'\n" +
                            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                            "                                            <line x1='400' y1='256' x2='112' y2='256'\n" +
                            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                            "                                        </svg>\n" +
                            "                                    </button>" +
                            "</div>\n" +


                            "                                        <div class=\"myFav " + result.data[i].id + "\">\n" +
                            "                                        <button class=\"card__favorite \" type=\"button\" onclick='addFavors(" + result.data[i].id + ")'>\n" +
                            "                                            <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"512\" height=\"512\" viewBox=\"0 0 512 512\">\n" +
                            "                                                <path d=\"M352.92,80C288,80,256,144,256,144s-32-64-96.92-64C106.32,80,64.54,124.14,64,176.81c-1.1,109.33,86.73,187.08,183,252.42a16,16,0,0,0,18,0c96.26-65.34,184.09-143.09,183-252.42C447.46,124.14,405.68,80,352.92,80Z\" style=\"fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px\"></path>\n" +
                            "                                            </svg>\n" +
                            "                                        </button>\n" +
                            "                                        </div>\n" +
                            "\t\t\t\t\t\t\t\t</div>\n" +
                            "\t\t\t\t\t\t\t</div>\n" +
                            "\t\t\t\t\t\t</div>";
                    }
                }
                $("#myCatalog").html(str);
            }
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
    checkFavors();
    checkCars();
    checkBuying();
}

function getQueryString(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]);
    return null;
}

function getDetails(cid) {
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/shop/getCommodity?cid=" + cid,
        async: false,
        success: function (result) {
            if (result.code === 1) {

                let str = "<div class=\"details__cover\">\n" +
                    "\t\t\t\t\t\t\t\t<img src=\"data:image/png;base64," + result.data.pic + "\" alt=\"\">\n" +
                    "\t\t\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t<div class=\"details__wrap\">\n" +
                    "\t\t\t\t\t\t\t\t<h1 class=\"details__title\">" + result.data.commodityName + "</h1>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t<ul class=\"details__list\">\n" +
                    "\t\t\t\t\t\t\t\t\t<li><span>作者:</span> " + result.data.creator + "</li>\n" +
                    "\t\t\t\t\t\t\t\t\t<li><span>发布时间:</span>" + result.data.cdate + "</li>\n" +
                    "\t\t\t\t\t\t\t\t\t<li><span>最后更新:</span> " + result.data.udate + "</li>\n" +
                    "\t\t\t\t\t\t\t\t\t<li><input type='hidden' id='creator' value='" + result.data.creatorId + "'> </li>\n" +
                    "\t\t\t\t\t\t\t\t\t<li><input type='hidden' id='creatorName' value='" + result.data.creator + "'> </li>\n" +
                    "\t\t\t\t\t\t\t\t</ul>\n" +
                    "\t\t\t\t\t\t\t</div>";
                $(".details__head").html(str);
                let price = "<span>￥" + result.data.price + "</span>";
                $(".details__price").html(price);
                let introduction = "<p>" + result.data.introduction + "</p>";
                $("#introduction").after(introduction);
                let btn = "<div class=\"myCar c" + result.data.id + "\">\n" +
                    // "                                        <button class=\"card__favorite \" type=\"button\" onclick='addCar(" + result.data.id + ")'>购物车</button>\n" +

                    "<button class=\"card__favorite\" type=\"button\" onclick='addCar(" + result.data.id + ")'>\n" +
                    "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                    "                                             viewBox='0 0 512 512'>\n" +
                    "                                            <line x1='256' y1='112' x2='256' y2='400'\n" +
                    "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                    "                                            <line x1='400' y1='256' x2='112' y2='256'\n" +
                    "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                    "                                        </svg>\n" +
                    "                                    </button>" +


                    "                                        </div>" +
                    "                                        <div class=\"myFav " + result.data.id + "\">\n" +
                    "                                        <button class=\"card__favorite \" type=\"button\" onclick='addFavors(" + result.data.id + ")'>\n" +
                    "                                            <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"512\" height=\"512\" viewBox=\"0 0 512 512\">\n" +
                    "                                                <path d=\"M352.92,80C288,80,256,144,256,144s-32-64-96.92-64C106.32,80,64.54,124.14,64,176.81c-1.1,109.33,86.73,187.08,183,252.42a16,16,0,0,0,18,0c96.26-65.34,184.09-143.09,183-252.42C447.46,124.14,405.68,80,352.92,80Z\" style=\"fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px\"></path>\n" +
                    "                                            </svg>\n" +
                    "                                        </button>\n" +
                    "                                        </div>";
                let buy = "<div class=\"myBuy details__buy b" + result.data.id + "\">\n" +
                    "<button type=\"button\" onclick=\"if(checkUser()){VIPBuy(" + result.data.id + ")}\">直接购买</button>\n" +
                    "</div>";
                $(".details__actions").html(btn);
                $(".details__actions").after(buy);

            }
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
    checkBuying();

    return true;
}

function removeCars(cid) {
    let uid = checkUser();
    if (uid) {
        if (confirm("确定删除？")) {
            $.ajax({
                type: "GET",
                url: "http://121.36.101.188:8080/api/v1/car/removeCar?uid=" + uid + "&cid=" + cid,
                async: false,
                success: function (result) {
                    if (result.code === 1) {
                        getCar();
                    }
                },
                error: function (errorMsg) {
                    alert("删除失败");
                }
            });

        }
    }
}

function deleteFavors(cid) {
    let uid = checkUser();
    if (uid) {
        if (confirm("确定删除？")) {
            $.ajax({
                type: "GET",
                url: "http://121.36.101.188:8080/api/v1/favor/deleteFavors?uid=" + uid + "&cid=" + cid,
                async: false,
                success: function (result) {
                    if (result.code === 1) {
                        getFavors();
                    }
                },
                error: function (errorMsg) {
                    alert("删除失败");
                }
            });

        }
    }
}

function reFavors(cid) {
    let uid = checkUser();
    if (uid) {
        if (confirm("取消收藏？")) {
            $.ajax({
                type: "GET",
                url: "http://121.36.101.188:8080/api/v1/favor/deleteFavors?uid=" + uid + "&cid=" + cid,
                async: false,
                success: function (result) {
                    if (result.code === 1) {
                        commodity();
                    }
                },
                error: function (errorMsg) {
                    alert("取消失败");
                }
            });

        }
    }
}

function reCars() {
    let uid = checkUser();
    if (uid) {
        if (confirm("确定全部删除？")) {
            $.ajax({
                type: "GET",
                url: "http://121.36.101.188:8080/api/v1/car/removeAllCars?uid=" + uid,
                async: false,
                success: function (result) {
                    if (result.code === 1) {
                        getCar();
                    }
                },
                error: function (errorMsg) {
                    alert("删除失败");
                }
            });

        }
    }
}

function chat() {
    $("#fill").show();
    $("#chat").show();
    document.onkeydown = function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            $("#send").click();
        }
    }
}

function pay() {
    let uid = checkUser();
    let cid = $(".info").val();
    if (uid && cid) {
        if (confirm("确认购买？")) {
            let clear = true;
            $.ajax({
                type: "POST",
                url: "http://121.36.101.188:8080/pay/confirm?uid=" + uid + "&cid=" + cid + "&clear=" + clear,
                async: false,
                contentType: 'application/json',
                dateType: "json",
                success: function (result) {
                    if (result.code === 1) {
                        let href = result.data;
                        $("body").html(href);
                    } else alert("支付失败!");

                },
                error: function (errorMsg) {
                    alert("支付失败");
                }
            });
        }
    } else {
        alert("赶紧挑选商品吧");
    }
}


function profile() {
    let user;
    if (localStorage.getItem("user")) {
        user = localStorage.getItem("user");

    } else if (sessionStorage.getItem("user")) {
        user = sessionStorage.getItem("user");
    }

    $("#username").val(JSON.parse(user).username);
    $("#user").val(JSON.parse(user).phoneNo);
    $("#trueName").val(JSON.parse(user).trueName);
    $("#idCard").val(JSON.parse(user).idCard);
    $("#aliPay").val(JSON.parse(user).aliPayId);
    $("#cDate").val(JSON.parse(user).createDate);
    let uid = JSON.parse(user).id;


    if (JSON.parse(user).roleId === 1) {
        $("#creator").val("尚未成为设计师");
        $("#creatorA").html("<a href='creator.html' style='font-size: 14px;cursor: pointer;margin-left: 20px'>点此开通</a>");
    } else if (JSON.parse(user).roleId === 2) {
        $("#creator").val("等待审核结果(请检查信息是否完善)");
    } else if (JSON.parse(user).roleId === 3) {
        $("#creator").val("已经成为设计师");
        $("#creatorA").html("<span style='font-size: 14px;margin-left: 20px;color: #795ab0;'>总收益:￥" + JSON.parse(user).designerIncome + "</span>" + "<span style='font-size: 10px;float:right;color: red;cursor: pointer;' onclick='changeRole(" + uid + "," + 1 + "," + JSON.parse(user).aliPayId + ")'>取消身份</span>");
    } else {

    }
    if (!JSON.parse(user).membership) {
        $("#vip").val("尚未开通会员");
        $("#vipA").html("<a href='javascript:;' onclick=\" confirmVip() \" style='font-size: 14px;cursor: pointer;margin-left: 20px'>点此开通</a>");
    } else {
        $("#vip").val("已开通会员");
        $("#vipA").html("<span style='font-size: 14px;margin-left: 20px;color: #795ab0'>已节约:￥" + JSON.parse(user).membershipIncome + "</span>");
    }


    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/shop/getByCreator?uid=" + uid,
        async: false,
        success: function (result) {
            let str = '<td></td><td></td><td><h2>您还没有作品！</h2></td><td></td>';
            if (result.code === 1) {
                if (result.data.length) {
                    str = '';
                    for (let i = 0; i < result.data.length; i++) {
                        let status;
                        if (result.data[i].statusId === 1) {
                            status = "审核中";
                        }
                        if (result.data[i].statusId === 2) {
                            status = "已上架";
                        }
                        if (result.data[i].statusId === 3) {
                            status = "<span style='color: red'>已下架</span>";
                        }

                        str += "<tr>\n" +
                            "                                    <td>" + result.data[i].id + "</td>\n" +
                            "                                    <td>\n" +
                            "                                        <div class=\"profile__img\">\n" +
                            "                                            <img src=\"data:image/png;base64," + result.data[i].pic + "\" alt=\"\">\n" +
                            "                                        </div>\n" +
                            "                                    </td>\n" +
                            "                                    <td>" + result.data[i].commodityName + "</td>\n" +
                            "                                    <td>" + result.data[i].udate + "</td>\n" +
                            "                                    <td><span class=\"profile__price\">￥" + result.data[i].price + "</span></td>\n" +
                            "                                    <td><span class=\"profile__status\">" + status + "</span></td>\n" +
                            "                                    <td><button class=\"profile__delete\" type=\"button\" onclick='changeStatus(" + result.data[i].id + ",3)'>\n" +
                            "                                            <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                            "                                                 viewBox='0 0 512 512'>\n" +
                            "                                                <line x1='368' y1='368' x2='144' y2='144'\n" +
                            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                            "                                                <line x1='368' y1='144' x2='144' y2='368'\n" +
                            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                            "                                            </svg>\n" +
                            "                                        </button>\n" +
                            "                                    </td>\n" +
                            "                                </tr>";
                    }
                }
                $("#myCommodity").html(str);
            }
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
    getBuying();
}

function checkMessage() {
    let uid = checkUser();
    if (uid) {
        $.ajax({
            type: "GET",
            url: "http://121.36.101.188:8080/api/v1/message/getMessage?uid=" + uid,
            async: false,
            success: function (result) {
                if (result.code === 1) {
                    if (result.data.length) {
                        let str = "";
                        console.log(result.data);
                        for (let i = 0; i < result.data.length; i++) {
                            if ((!result.data[i].reading) && result.data[i].fromUid !== uid) {

                                str += `<tr onclick='chat();connect(${result.data[i].fromUid},"${result.data[i].fromName}");'>\n` +
                                    "                                    <td>" + result.data[i].fromName + "</td>\n" +
                                    "                                    <td colspan='2'>" + result.data[i].cdate + "</td> \n" +
                                    "                                    <td colspan='2'>" + result.data[i].content + "</td>\n" +
                                    "                                    <td id='m" + result.data[i].fromUid + "'><span class=\"profile__status profile__status--cenceled\">未处理</span></td>\n" +
                                    "                                    <td></td>\n" +
                                    "                                    </tr>";

                            }
                        }
                        $("#adminMessage").html(str);
                    }
                }
            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });
    }

}


function checkFileType(_this) {
    let $this = $(_this);
    let acceptType = $this.attr('accept');
    let selectedFile = $this.val();
    let fileType = selectedFile.substring(selectedFile.indexOf('.') + 1, selectedFile.length);
    let location = acceptType.indexOf(fileType);
    if (location > -1) {

    } else {
        alert('请选择' + acceptType + '格式文件');
        $this.val("");
    }
}

function changeRole(uid, roleId, account) {
    let user;
    if (localStorage.getItem("user")) {
        user = localStorage.getItem("user");

    } else if (sessionStorage.getItem("user")) {
        user = sessionStorage.getItem("user");
    }
    let x = JSON.parse(user).roleId;
    let str;

    if (roleId === 2) {
        str = "确认提交？"
    } else if (roleId === 3) {
        str = "确定通过审核？"
    } else if (roleId === 1) {
        str = "确认取消设计师身份？"
    }
    if (confirm(str)) {
        $.ajax({
            type: "POST",
            url: "http://121.36.101.188:8080/api/v1/user/changeRole?uid=" + uid + "&roleId=" + roleId + "&aliPayId=" + account,
            async: false,
            contentType: 'application/json',
            dateType: "json",
            success: function (result) {
                if (result.code === 1) {
                    if (x === 4) {
                        alert("成功");
                        getUser();
                    } else if (roleId === 1) {
                        alert("已取消");
                        if (changeInfo()) profile();
                    } else {
                        alert("申请成功！请尽快完善信息并等待审核");
                        if (changeInfo())
                            window.location.href = "profile.html";
                    }

                } else alert("失败!");

            },
            error: function (errorMsg) {
                alert("失败");
            }

        });
    }
}

function checkPower() {
    let user;
    if (localStorage.getItem("user")) {
        user = localStorage.getItem("user");

    } else if (sessionStorage.getItem("user")) {
        user = sessionStorage.getItem("user");
    }
    if (user) {
        if (JSON.parse(user).roleId === 1) {
            let str = "<a href=\"creator.html\"><span class=\"upload\"></span><b>赚钱</b></a>";
            $(".service").before(str);
        } else if (JSON.parse(user).roleId === 2) {
            let str = "<a href=\"javascript:;\"><span class=\"upload\"></span><b>审核中</b></a>";
            $(".service").before(str);
        } else if (JSON.parse(user).roleId === 3) {
            let str = "<a href=\"upload.html\" ><span class=\"upload upload-h\"></span><b>赚钱</b></a>";
            $(".service").before(str);
        }
        if (!JSON.parse(user).membership) {
            let str = "<a href=\"javascript:;\" onclick=\" confirmVip() \"><span class=\"vip\"></span><b>会员</b></a>";
            $(".service").before(str);

        } else {
            let str = "<a href=\"javascript:;\"><span class=\"vip vip-h\"></span><b>会员</b></a>";
            $(".service").before(str);
            let vip = "<a class=\"header__lang-btn\" href=\"javascript:;\" role=\"button\" id=\"dropdownMenuLang\"\n" +
                "                                   data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
                "                                    <img src=\"img/vip-h.png\" alt=\"\">\n" +
                "                                    <span>会员</span>\n" +
                "                                </a>";
            $(".header__lang").html(vip);
            let vipNu = vipNum(JSON.parse(user).id);
            $(".vipNum").html(vipNu);
        }
    } else {
        let str = "<a href=\"creator.html\" onclick=\"return checkUser();\"><span class=\"upload\"></span><b>赚钱</b></a><a href=\"javascript:;\" onclick=\" return checkUser(); \"><span class=\"vip\"></span><b>会员</b></a>";
        $(".service").before(str);
    }

}

function upload() {
    let uid = checkUser();
    let title = $(".title").val();
    let price = $(".price").val();
    let commodityName = $(".commodityName").val();
    let info = $(".info").val();
    let pv = $("#upload_ppt").val();
    let iv = $("#upload_img").val();
    if (title && price && commodityName && info && pv && iv) {
        let formData = new FormData();
        let ppt = $("#upload_ppt");
        formData.append("file", ppt[0].files[0]);

        let imgs = $("#upload_img");
        for (let i = 0; i < imgs[0].files.length; i++) {
            formData.append("file", imgs[0].files[i]);
        }
        formData.append("creator", uid * 1);
        formData.append("title", title);
        formData.append("price", price * 1);
        formData.append("commodityName", commodityName);
        formData.append("introduction", info);
        $.ajax({
            url: "http://121.36.101.188:8080/api/v1/shop/upload",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.code === 1) {
                    alert("上传成功!审核通过后会上架");
                    window.location.href = "index.html";
                } else {
                    alert("失败！");
                }

            },
            error: function (errorMsg) {
                alert("失败");
            }
        });


    } else {
        alert("请输入全部信息！");
    }
}

function getBuying() {
    let uid = checkUser();
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/user/getBuying?uid=" + uid,
        async: false,
        contentType: 'application/json',
        dateType: "json",
        success: function (result) {
            if (result.code === 1) {
                let str = "<td></td><td></td><td><h2>赶紧去购买！</h2></td><td></td><td></td><td></td>";
                if (result.data.length) {
                    str = "";

                    for (let i = 0; i < result.data.length; i++) {
                        str += "<tr>\n" +
                            "                                    <td>\n" +
                            "                                        <div class=\"profile__img\">\n" +
                            "                                            <img src=\"data:image/png;base64," + result.data[i].pic + "\" alt=\"\">\n" +
                            "                                        </div>\n" +
                            "                                    </td>\n" +
                            "                                    <td>" + result.data[i].creator + "</td>\n" +
                            "                                    <td>" + result.data[i].commodityName + "</td>\n" +
                            "                                    <td>" + result.data[i].udate + "</td>\n" +
                            "                                    <td><a href='javascript:;' onclick='downloadBuying(" + result.data[i].id + ")'>立即下载</a></td>\n" +
                            "                                    <td></td>\n" +
                            "                                </tr>";
                    }
                }
                $("#myBuying").html(str);
            } else alert("失败!");

        },
        error: function (errorMsg) {
            alert("失败");
        }

    });
}

function admin(page) {

    let maxPage = parseInt($("#pageSize").html());
    let pag = "";
    if (page - 1 > 0) {
        pag = pag + "<li class=\"paginator__item paginator__item--prev\">\n" +
            "                                    <a href=\"#\" onclick='admin(" + (page - 1) + ")'>\n" +
            "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
            "                                             viewBox='0 0 512 512'>\n" +
            "                                            <polyline points='244 400 100 256 244 112'\n" +
            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                            <line x1='120' y1='256' x2='412' y2='256'\n" +
            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                        </svg>\n" +
            "                                    </a>\n" +
            "                                </li>\n"
    }
    if (page >= 1 && page <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item paginator__item--active\"><a href=\"#\">" + page + "</a></li>\n"
    }
    if (page + 1 >= 1 && page + 1 <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item\"><a href=\"#\" onclick='admin(" + (page + 1) + ")'>" + (page + 1) + "</a></li>\n"
    }
    if (page + 2 >= 1 && page + 2 <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item\"><a href=\"#\" onclick='admin(" + (page + 2) + ")'>" + (page + 2) + "</a></li>\n"
    }
    if (page + 1 <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item paginator__item--next\">\n" +
            "                                    <a href=\"#\" onclick='admin(" + (page + 1) + ")'>\n" +
            "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
            "                                             viewBox='0 0 512 512'>\n" +
            "                                            <polyline points='268 112 412 256 268 400'\n" +
            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                            <line x1='392' y1='256' x2='100' y2='256'\n" +
            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                        </svg>\n" +
            "                                    </a>\n" +
            "                                </li>";
    }

    $("#myPage").html(pag);


    let pageNumber = page;
    let pageSize = 10;
    let statusId = 1;
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/shop/getAllCommodity?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&statusId=" + statusId,
        async: false,
        success: function (result) {
            let str = '<td></td><td></td><td><h2>没有待审核商品</h2></td><td></td>';
            if (result.code === 1) {
                if (result.data.length) {
                    str = '';
                    for (let i = 0; i < result.data.length; i++) {
                        str += "<tr>\n" +
                            "                                    <td>" + result.data[i].id + "</td>\n" +
                            "                                    <td>\n" +
                            "                                        <div class=\"profile__img\">\n" +
                            "                                            <img src=\"data:image/png;base64," + result.data[i].pic + "\" alt=\"\">\n" +
                            "                                        </div>\n" +
                            "                                    </td>\n" +
                            "                                    <td>" + result.data[i].commodityName + "</td>\n" +
                            "                                    <td>" + result.data[i].udate + "</td>\n" +
                            "                                    <td><span class=\"profile__price\">￥" + result.data[i].price + "</span></td>\n" +
                            "                                    <td><a href=\"examine.html?cid=" + result.data[i].id + "\">前往审核</a></td>\n" +
                            "                                    <td></td>\n" +
                            "                                </tr>";
                    }
                }
                $("#myWork").html(str);
            }
        },
        error: function (errorMsg) {
            alert("获取失败");
        },
    });

    if (getSize(2, 10)) getAC(1)
    getUser();
}

function examine(cid) {
    let creatorId = $("#creator").val();
    let creatorName = $("#creatorName").val();
    let download = "<a href=\"javascript:;\" onclick='downloadBuying(" + cid + ")'>\n" +
        "                                <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512' viewBox='0 0 512 512'>\n" +
        "                                    <path d='M336,176h40a40,40,0,0,1,40,40V424a40,40,0,0,1-40,40H136a40,40,0,0,1-40-40V216a40,40,0,0,1,40-40h40'\n" +
        "                                          style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
        "                                    <polyline points='176 272 256 352 336 272'\n" +
        "                                              style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
        "                                    <line x1='256' y1='48' x2='256' y2='336'\n" +
        "                                          style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
        "                                </svg>\n" +
        "                                <b>下载文件</b>\n" +
        "                                </a>";
    let btn = "<div class=\"card__buy\">\n" +
        "                                        <button  type=\"button\" " + `onclick='chat();connect(${creatorId},"${creatorName}");'` + ">联系作者</button>\n" +
        "                                        </div>" +
        "                                        <button class=\"card__favorite \" type=\"button\" onclick='changeStatus(" + cid + ",3)'>\n" +
        "\t\t\t\t\t\t\t\t\t\t<svg xmlns='http://www.w3.org/2000/svg' width='512' height='512' viewBox='0 0 512 512'><path d='M112,112l20,320c.95,18.49,14.4,32,32,32H348c17.67,0,30.87-13.51,32-32l20-320' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='80' y1='112' x2='432' y2='112' style='stroke-linecap:round;stroke-miterlimit:10;stroke-width:32px'/><path d='M192,112V72h0a23.93,23.93,0,0,1,24-24h80a23.93,23.93,0,0,1,24,24h0v40' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='256' y1='176' x2='256' y2='400' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='184' y1='176' x2='192' y2='400' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='328' y1='176' x2='320' y2='400' style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/></svg>\n" +
        "                                        </button>\n";
    let buy =
        "<button type=\"button\" onclick='changeStatus(" + cid + ",2)'>审核通过</button>";
    $("#download").html(download);
    $(".details__actions").html(btn);
    $(".details__buy").html(buy);
}

function changeStatus(cid, status) {
    let roleId;
    if (localStorage.getItem("user")) {
        roleId = JSON.parse(localStorage.getItem("user")).roleId;
    } else if (sessionStorage.getItem("user")) {
        roleId = JSON.parse(sessionStorage.getItem("user")).roleId;
    } else {
    }
    let str;
    if (status === 2) {
        str = "确认通过审核？"
    } else {
        str = "确认下架商品？"
    }
    if (confirm(str)) {
        $.ajax({
            type: "POST",
            url: "http://121.36.101.188:8080/api/v1/shop/changeStatus?cid=" + cid + "&statusId=" + status,
            async: false,
            contentType: 'application/json',
            dateType: "json",
            success: function (result) {
                if (result.code === 1) {
                    alert("成功");
                    if (roleId === 4) {
                        window.location.href = "admin.html";
                    } else {
                        profile();
                    }
                } else {
                    alert("失败");
                }

            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        })
    }
}

function downloadBuying(cid) {
    window.location.href = "http://121.36.101.188:8080/api/v1/shop/download?cid=" + cid;
}

function confirmVip() {
    let uid = checkUser();
    if (confirm("确认开通会员？")) {
        $.ajax({
            type: "POST",
            url: "http://121.36.101.188:8080/pay/confirmVIP?uid=" + uid,
            async: false,
            success: function (result) {
                if (result.code === 1) {
                    let href = result.data;
                    $("body").html(href);
                } else {
                    alert("失败");
                }

            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });
    }
}

function searchInfo(Info) {
    $(".header__input").val(Info);
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/shop/search?param=" + Info,
        async: false,
        success: function (result) {
            let str = '<h2 class="section__title">空空如也</h2>';
            if (result.code === 1) {
                if (result.data.length) {
                    str = '';
                    for (let i = 0; i < result.data.length; i++) {
                        str += "<div class=\"col-12 col-sm-6 col-xl-4 \">\n" +
                            "\t\t\t\t\t\t\t<div class=\"card\">\n" +
                            "\t\t\t\t\t\t\t\t<a href=\"details.html?cid=" + result.data[i].id + "\" class=\"card__cover\">\n" +
                            "\t\t\t\t\t\t\t\t\t<img src=\"data:image/png;base64," + result.data[i].pic + "\" alt=\"\">\n" +
                            "\t\t\t\t\t\t\t\t</a>\n" +
                            "\n" +
                            "\t\t\t\t\t\t\t\t<div class=\"card__title\">\n" +
                            "\t\t\t\t\t\t\t\t\t<h3><a href=\"details.html?cid=" + result.data[i].id + "\">" + result.data[i].commodityName + "</a></h3>\n" +
                            "\t\t\t\t\t\t\t\t\t<span>￥" + result.data[i].price + "</span>\n" +
                            "\t\t\t\t\t\t\t\t</div>\n" +
                            "\n" +
                            "\t\t\t\t\t\t\t\t<div class=\"card__actions\">\n" +
                            // "<div class=\"myCar card__buy c" + result.data[i].id + "\">\n" +
                            // "                                        <button  type=\"button\" onclick='addCar(" + result.data[i].id + ")'>购买</button>\n" +
                            // "                                        </div>" +


                            "    <div class=\"myBuy card__buy b" + result.data[i].id + "\">\n" +
                            "                                        <button type=\"button\" onclick=\"if(checkUser()){VIPBuy(" + result.data[i].id + ")}\" >购买</button>\n" +
                            "                                        </div>" +
                            "<div class=\"myCar c" + result.data[i].id + "\">\n" +
                            "<button class=\"card__favorite\" type=\"button\" onclick='addCar(" + result.data[i].id + ")'>\n" +
                            "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                            "                                             viewBox='0 0 512 512'>\n" +
                            "                                            <line x1='256' y1='112' x2='256' y2='400'\n" +
                            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                            "                                            <line x1='400' y1='256' x2='112' y2='256'\n" +
                            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                            "                                        </svg>\n" +
                            "                                    </button>" +
                            "</div>\n" +


                            "                                        <div class=\"myFav " + result.data[i].id + "\">\n" +
                            "                                        <button class=\"card__favorite \" type=\"button\" onclick='addFavors(" + result.data[i].id + ")'>\n" +
                            "                                            <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"512\" height=\"512\" viewBox=\"0 0 512 512\">\n" +
                            "                                                <path d=\"M352.92,80C288,80,256,144,256,144s-32-64-96.92-64C106.32,80,64.54,124.14,64,176.81c-1.1,109.33,86.73,187.08,183,252.42a16,16,0,0,0,18,0c96.26-65.34,184.09-143.09,183-252.42C447.46,124.14,405.68,80,352.92,80Z\" style=\"fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px\"></path>\n" +
                            "                                            </svg>\n" +
                            "                                        </button>\n" +
                            "                                        </div>\n" +
                            "\t\t\t\t\t\t\t\t</div>\n" +
                            "\t\t\t\t\t\t\t</div>\n" +
                            "\t\t\t\t\t\t</div>";
                    }
                }
                $("#myCatalog").html(str);
            }
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
    checkFavors();
    checkCars();
    checkBuying();

}

function VIPBuy(cid) {
    let user;
    if (localStorage.getItem("user")) {
        user = localStorage.getItem("user");

    } else if (sessionStorage.getItem("user")) {
        user = sessionStorage.getItem("user");
    }
    let uid = JSON.parse(user).id;
    if (confirm("确认购买？")) {
        if (!JSON.parse(user).membership) {
            buyOne(uid, cid);
        } else {
            $.ajax({
                type: "POST",
                url: "http://121.36.101.188:8080/pay/VIPBuy?uid=" + uid + "&cid=" + cid,
                async: false,
                success: function (result) {
                    if (result.code === 1) {
                        alert("成功使用每日会员特权！");
                        window.location.href = "profile.html"
                    } else {
                        buyOne(uid, cid);
                    }
                },
                error: function (errorMsg) {
                    alert("获取失败");
                }
            });
        }
    }
}

function buyOne(uid, cid) {
    $.ajax({
        type: "POST",
        url: "http://121.36.101.188:8080/pay/confirm?clear=" + false + "&uid=" + uid + "&cid=" + cid,
        async: false,
        success: function (result) {
            if (result.code === 1) {
                let href = result.data;
                $("body").html(href);
            } else alert("支付失败!");
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
}

function check_img(a) {

    /*    let file = $("#upload_img")[0].files[0];
        let reader = new FileReader();
        reader.onload = function (e) {
            let data = e.target.result;
            let image = new Image();
            image.onload = function () {
                let width = image.width;
                let height = image.height;
                if (width !== 640 || height !== 400) {
                    alert('请上传640*400像素的图片');
                    $(a).val("");
                }
            };
            image.src = data;
        };
        reader.readAsDataURL(file);*/

}

function changePwd() {
    let json = {
        "phoneNumber": $("#phone").val(),
        "password": $("#pwd1").val()
    };
    $.ajax({
        type: "POST",
        data: JSON.stringify(json),
        url: "http://121.36.101.188:8080/api/v1/user/changePwd",
        async: false,
        contentType: 'application/json',
        dateType: "json",
        success: function (result) {
            if (result.code === 1) {
                alert("修改成功！");
                quit();
            } else alert("修改失败!");
        },
        error: function (errorMsg) {
            alert("修改失败");
        }
    });
}

function changeInfo() {
    let uid;
    let storage;
    if (localStorage.getItem("user")) {
        uid = JSON.parse(localStorage.getItem("user")).id;
        storage = 1;
    } else if (sessionStorage.getItem("user")) {
        uid = JSON.parse(sessionStorage.getItem("user")).id;
        storage = 2;
    }
    $.ajax({
        type: "POST",
        url: "http://121.36.101.188:8080/api/v1/user/info?uid=" + uid,
        async: false,
        success: function (result) {
            if (result.code === 1) {
                if (storage === 1) {
                    localStorage.clear();
                    localStorage.setItem("user", JSON.stringify(result.data));
                } else if (storage === 2) {
                    sessionStorage.clear();
                    sessionStorage.setItem("user", JSON.stringify(result.data));
                }
            } else alert("获取失败!");
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
    return true;
}

function getSize(status, pageSize) {
    let num = 0;
    let maxPage = 1;
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/shop/getSize?statusId=" + status,
        async: false,
        success: function (result) {
            if (result.code === 1) {
                num = result.data;
            } else alert("失败!");
        },
        error: function (errorMsg) {
            alert("失败");
        }
    });

    if (num) {
        maxPage = Math.ceil(num / pageSize)
    }
    if (status === 1) {
        $("#pageSize").html("" + maxPage + "");
    } else {
        $("#CPageSize").html("" + maxPage + "");
    }


    return true;
}

function getUser() {
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/user/getAllUser",
        async: false,
        success: function (result) {
            let str = '';
            if (result.code === 1) {
                if (result.data.length) {
                    str = '';
                    for (let i = 0; i < result.data.length; i++) {
                        if (result.data[i].roleId !== 4 && !result.data[i].logout) {
                            let vip;
                            let name;
                            let idCard;
                            let aliPayId;
                            let roleId;
                            let check;
                            if (result.data[i].membership) {
                                vip = "是"
                            } else {
                                vip = "否"
                            }
                            if (result.data[i].trueName) {
                                name = result.data[i].trueName
                            } else {
                                name = "无"
                            }
                            if (result.data[i].idCard) {
                                idCard = result.data[i].idCard
                            } else {
                                idCard = "无"
                            }
                            if (result.data[i].aliPayId) {
                                aliPayId = result.data[i].aliPayId
                            } else {
                                aliPayId = "无"
                            }
                            if (result.data[i].roleId === 1) {
                                roleId = "用户";
                                check = "无"
                            } else if (result.data[i].roleId === 2) {
                                roleId = "等待审核";
                                check = "<a href=\"javascript:;\" onclick='changeRole(" + result.data[i].id + "," + 3 + "," + aliPayId + ")'>通过审核</a>"
                            } else if (result.data[i].roleId === 3) {
                                roleId = "设计师";
                                check = "<a href=\"javascript:;\" onclick='changeRole(" + result.data[i].id + "," + 1 + "," + aliPayId + ")'>取消身份</a>"
                            }
                            str += "<tr>\n" +
                                "                                    <td>" + result.data[i].id + "</td>\n" +
                                "                                    <td>\n" +
                                "                                        " + result.data[i].username + "" +
                                "                                    </td>\n" +
                                "                                    <td>" + result.data[i].phoneNo + "</td>\n" +
                                "                                    <td>" + result.data[i].createDate + "</td>\n" +
                                "                                    <td>" + vip + "</td>\n" +
                                "                                    <td>" + name + "</span></td>\n" +
                                "                                    <td>" + idCard + "</td>\n" +
                                "                                    <td>" + aliPayId + "</td>\n" +
                                "                                    <td>" + roleId + "</td>\n" +
                                "                                    <td>" + check + "</td>\n" +
                                "                                    <td>\n" +
                                "                                        <button class=\"profile__delete\" type=\"button\" onclick='logout(" + result.data[i].id + ")'>\n" +
                                "                                            <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                                "                                                 viewBox='0 0 512 512'>\n" +
                                "                                                <line x1='368' y1='368' x2='144' y2='144'\n" +
                                "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                                "                                                <line x1='368' y1='144' x2='144' y2='368'\n" +
                                "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                                "                                            </svg>\n" +
                                "                                        </button>\n" +
                                "                                    </td>\n" +
                                "                                </tr>"
                        }
                    }
                }
                $("#userList").html(str);
            }
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
}

function infoCheck() {
    let flag = true;
    let trueName = document.getElementById("trueName").value;
    let trueCheck = document.getElementById("trueCheck");
    let idCard = document.getElementById("idCard").value;
    let idCheck = document.getElementById("idCheck");

    if (!(/^[\u4e00-\u9fa5]{2,4}$/.test(trueName))) {
        trueCheck.innerHTML = "真实姓名有误";
        flag = false;
    } else {
        trueCheck.innerHTML = "";
    }
    if (!(/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(idCard))) {
        idCheck.innerHTML = '身份证号填写有误';
        flag = false;
    } else {
        idCheck.innerHTML = "";
    }

    return flag;
}

function updateInfo() {
    if (infoCheck()) {
        let username = $("#username").val();
        let phoneNo = $("#user").val();
        let trueName = $("#trueName").val();
        let idCard = $("#idCard").val();
        let aliPay = $("#aliPay").val();
        let uid = checkUser();
        let json = {
            id: uid,
            username: username,
            phoneNo: phoneNo,
            aliPayId: aliPay,
            idCard: idCard,
            trueName: trueName
        };
        $.ajax({
            type: "POST",
            data: JSON.stringify(json),
            url: "http://121.36.101.188:8080/api/v1/user/changeInfo",
            async: false,
            contentType: 'application/json',
            dateType: "json",
            success: function (result) {
                if (result.code === 1) {
                    alert("保存成功");
                } else alert("获取失败!");
            },
            error: function (errorMsg) {
                alert("获取失败");
            },
            complete: function () {
                if (changeInfo()) profile();
            }
        });
    } else {
        alert("请检查信息")
    }
}

function logout(yid = 1) {
    if (confirm("确认注销？")) {
        let user;
        let uid;
        if (localStorage.getItem("user")) {
            user = localStorage.getItem("user");

        } else if (sessionStorage.getItem("user")) {
            user = sessionStorage.getItem("user");
        }
        let role = JSON.parse(user).roleId;
        let xid = JSON.parse(user).id;
        if (role === 4) {
            uid = yid;
        } else {
            uid = xid;
        }
        $.ajax({
            type: "POST",
            url: "http://121.36.101.188:8080/api/v1/user/logout?id=" + uid,
            async: false,
            success: function (result) {
                if (result.code === 1) {
                    alert("已注销!");
                    if (role === 4) {
                        getUser();
                    } else {
                        quit();
                    }
                } else alert("获取失败!");
            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });
    }
}

function vipNum(uid) {
    let num;
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/mem/getCounts?id=" + uid,
        async: false,
        success: function (result) {
            if (result.code === 1) {
                num = result.data;
            } else alert("失败!");
        },
        error: function (errorMsg) {
            alert("失败");
        }
    });
    return parseInt(num);
}

function getAC(page) {
    let maxPage = parseInt($("#CPageSize").html());
    let pag = "";
    if (page - 1 > 0) {
        pag = pag + "<li class=\"paginator__item paginator__item--prev\">\n" +
            "                                    <a href=\"#\" onclick='getAC(" + (page - 1) + ")'>\n" +
            "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
            "                                             viewBox='0 0 512 512'>\n" +
            "                                            <polyline points='244 400 100 256 244 112'\n" +
            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                            <line x1='120' y1='256' x2='412' y2='256'\n" +
            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                        </svg>\n" +
            "                                    </a>\n" +
            "                                </li>\n"
    }
    if (page >= 1 && page <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item paginator__item--active\"><a href=\"#\">" + page + "</a></li>\n"
    }
    if (page + 1 >= 1 && page + 1 <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item\"><a href=\"#\" onclick='getAC(" + (page + 1) + ")'>" + (page + 1) + "</a></li>\n"
    }
    if (page + 2 >= 1 && page + 2 <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item\"><a href=\"#\" onclick='getAC(" + (page + 2) + ")'>" + (page + 2) + "</a></li>\n"
    }
    if (page + 1 <= maxPage) {
        pag = pag + "                                <li class=\"paginator__item paginator__item--next\">\n" +
            "                                    <a href=\"#\" onclick='getAC(" + (page + 1) + ")'>\n" +
            "                                        <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
            "                                             viewBox='0 0 512 512'>\n" +
            "                                            <polyline points='268 112 412 256 268 400'\n" +
            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                            <line x1='392' y1='256' x2='100' y2='256'\n" +
            "                                                  style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:48px'/>\n" +
            "                                        </svg>\n" +
            "                                    </a>\n" +
            "                                </li>";
    }

    $("#CPage").html(pag);


    let pageNumber = page;
    let pageSize = 10;


    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/shop/getAllCommodity?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&statusId=" + 2,
        async: false,
        success: function (result) {
            let str = '<td></td><td></td><td><h2>暂无作品！</h2></td><td></td>';
            if (result.code === 1) {
                if (result.data.length) {
                    str = '';
                    for (let i = 0; i < result.data.length; i++) {

                        str += "<tr>\n" +
                            "                                    <td>" + result.data[i].id + "</td>\n" +
                            "                                    <td>\n" +
                            "                                        <div class=\"profile__img\">\n" +
                            "                                            <img src=\"data:image/png;base64," + result.data[i].pic + "\" alt=\"\">\n" +
                            "                                        </div>\n" +
                            "                                    </td>\n" +
                            "                                    <td>" + result.data[i].commodityName + "</td>\n" +
                            "                                    <td>" + result.data[i].udate + "</td>\n" +
                            "                                    <td><span class=\"profile__price\">￥" + result.data[i].price + "</span></td>\n" +
                            "                                    <td><span class=\"profile__status\">" + result.data[i].creator + "</span></td>\n" +
                            "                                    <td><button class=\"profile__delete\" type=\"button\" onclick='changeStatus(" + result.data[i].id + ",3)'>\n" +
                            "                                            <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512'\n" +
                            "                                                 viewBox='0 0 512 512'>\n" +
                            "                                                <line x1='368' y1='368' x2='144' y2='144'\n" +
                            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                            "                                                <line x1='368' y1='144' x2='144' y2='368'\n" +
                            "                                                      style='fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/>\n" +
                            "                                            </svg>\n" +
                            "                                        </button>\n" +
                            "                                    </td>\n" +
                            "                                </tr>";
                    }
                }
                $("#myCommodity").html(str);
            }
        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
}

function getAdmin() {
    $.ajax({
        type: "GET",
        url: "http://121.36.101.188:8080/api/v1/user/getAdmin",
        async: false,
        success: function (result) {
            let str = "";
            if (result.code === 1) {
                if (result.data.length) {
                    for (let i = 0; i < result.data.length; i++) {
                        // str+="<div class=\"admin-item\" onclick=\"if(checkUser()){chat();connect("+result.data[i].id+','+result.data[i].username+");}\">"+result.data[i].username+"</div>";
                        str += `<div class="admin-item" onclick="if(checkUser()){chat();connect(${result.data[i].id},'${result.data[i].username}');}">${result.data[i].username}</div>`;
                    }
                }
            }
            $(".admin-list").html(str);

        },
        error: function (errorMsg) {
            alert("获取失败");
        }
    });
}


function historyMessage() {
    let uid = checkUser();
    if (uid) {
        $.ajax({
            type: "GET",
            url: "http://121.36.101.188:8080/api/v1/message/getMessage?uid=" + uid,
            async: false,
            success: function (result) {
                if (result.code === 1) {
                    if (result.data.length) {
                        let str = "";
                        console.log(result.data);
                        for (let i = 0; i < result.data.length; i++) {
                            if (result.data[i].fromUid !== uid) {

                                str += `<tr onclick='chat();connect(${result.data[i].fromUid},"${result.data[i].fromName}");'>\n` +
                                    "                                    <td>" + result.data[i].fromName + "</td>\n" +
                                    "                                    <td colspan='2'>" + result.data[i].cdate + "</td> \n" +
                                    "                                    <td colspan='2'>" + result.data[i].content + "</td>\n" +
                                    "                                    <td></td>\n" +
                                    "                                    <td></td>\n" +
                                    "                                    </tr>";

                            }
                        }
                        $("#historyMessage").html(str);
                    }
                }
            },
            error: function (errorMsg) {
                alert("获取失败");
            }
        });
    }

}