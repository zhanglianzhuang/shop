package com.shop.common.service.Impl;

import com.shop.api.pojo.MessageDto;
import com.shop.api.service.UserService;
import com.shop.common.service.MessageService;
import com.shop.common.util.JsonUtil;
import com.shop.persist.entity.Message;
import com.shop.persist.mapper.MessageMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MessageServiceImpl implements MessageService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private UserService userService;

    private static final String MESSAGE_PREFIX = "message";
    private static final String CACHE_RESOURCE = ":";

    @Override
    public void saveMessage(MessageDto messageDto, LocalDate date) {
        String key = generateKey(date);
        messageDto.setCDATE(LocalDateTime.now());
        if (Boolean.TRUE.equals(messageDto.isReading())) {
            messageMapper.addMessage(generateMessage(messageDto));
            return;
        }
        redisTemplate.opsForList().rightPush(key + messageDto.getToUid(), JsonUtil.toJson(messageDto));
        redisTemplate.expire(key + messageDto.getToUid(), Duration.ofDays(2));
    }

    @Override
    public void saveMessage(LocalDate date) {
        String key = generateKey(date);
        Set<String> keys = redisTemplate.keys(key + "*");
        if (Objects.isNull(keys)) {
            log.info("no Message to save");
            return;
        }
        List<String> toSave = getMessageByKeys(keys);

        List<Message> messages = toSave.stream()
                .map(t -> JsonUtil.parse(t, MessageDto.class))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(this::generateMessage)
                .collect(Collectors.toList());
        messages.forEach(message -> messageMapper.addMessage(message));
    }

    @Override
    public List<MessageDto> getMessage(int uid) {
        List<Message> message = messageMapper.getMessage(uid);
        List<MessageDto> messageDto = message.stream().map(this::generateMessageDto).collect(Collectors.toList());
        String key = generateKey(LocalDate.now());
        Set<String> keys = redisTemplate.keys(key + "*");

        if (Objects.nonNull(keys)) {
            List<String> get = getMessageByKeys(keys);
            List<MessageDto> result = get.stream()
                    .map(t -> JsonUtil.parse(t, MessageDto.class))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .filter(t -> t.getFromUid() == uid || t.getToUid() == uid)
                    .sorted(Comparator.comparing(MessageDto::getCDATE))
                    .collect(Collectors.toList());
            result.addAll(messageDto);
            result.sort(Comparator.comparing(MessageDto::getCDATE));
            //已读
            Set<String> strings = new HashSet<>();
            strings.add(key + uid);
            List<String> messageByKeys = getMessageByKeys(strings);
            List<Message> save = messageByKeys.stream()
                    .map(t -> JsonUtil.parse(t, MessageDto.class))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .map(this::generateMessage)
                    .collect(Collectors.toList());
            save.forEach(t -> {
                t.setReading(true);
                messageMapper.addMessage(t);
            });
            redisTemplate.delete(key + uid);

            message.forEach(t -> {
                if (!t.isReading()) {
                    messageMapper.updateMessageForReading(t.getId());
                }
            });

            return setNames(result);
        }


        return setNames(messageDto);
    }

    @Override
    public boolean getUnReading(int uid) {
        //true==有未读  false==无
        Boolean b = redisTemplate.hasKey(generateKey(LocalDate.now()) + uid);
        if (Objects.isNull(b) || !b) {
            int result = messageMapper.selectUnReadingNum(uid);
            return result != 0;
        }
        return true;
    }

    private String generateKey(LocalDate date) {
        return new StringBuilder()
                .append(MESSAGE_PREFIX)
                .append(CACHE_RESOURCE)
                .append(date)
                .append(CACHE_RESOURCE)
                .toString();
    }

    private Message generateMessage(MessageDto messageDto) {
        Message message = new Message();
        BeanUtils.copyProperties(messageDto, message);
        return message;
    }

    private MessageDto generateMessageDto(Message message) {
        MessageDto messageDto = new MessageDto();
        BeanUtils.copyProperties(message, messageDto);

        return messageDto;
    }

    private List<String> getMessageByKeys(Set<String> keys) {
        List<String> get = new ArrayList<>();
        for (String k : keys) {
            Long size = redisTemplate.opsForList().size(k);
            if (Objects.isNull(size) || size == 0) {
                log.info("the key is null {}", k);
                continue;
            }
            List<String> range = redisTemplate.opsForList().range(k, 0, size);
            if (Objects.isNull(range)) {
                log.info("the key is null {}", k);
                continue;
            }
            get.addAll(range);
        }
        return get;
    }

    private List<MessageDto> setNames(List<MessageDto> messages) {
        messages.forEach(t -> {
            t.setToName(userService.getUser(t.getToUid()).getUsername());
            t.setFromName(userService.getUser(t.getFromUid()).getUsername());
        });
        return messages;
    }
}
