package com.shop.common.service;

import com.shop.api.pojo.MessageDto;

import java.time.LocalDate;
import java.util.List;

public interface MessageService {
    void saveMessage(MessageDto messageDto, LocalDate date);

    void saveMessage(LocalDate date);

    List<MessageDto> getMessage(int uid);

    boolean getUnReading(int uid);
}
