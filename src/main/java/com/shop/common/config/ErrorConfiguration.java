package com.shop.common.config;

import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * ErrorConfiguration
 *
 * @author zhang
 * @version 1.0
 * 2021/3/1 18:57
 */
@Configuration
public class ErrorConfiguration {
    @Bean
    WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer() {
        return (factory -> {
            ErrorPage errorPage404 = new ErrorPage(HttpStatus.NOT_FOUND, "/api/v1/sys/404");
            factory.addErrorPages(errorPage404);
        });
    }
}
