package com.shop.common.config;

import lombok.Getter;
import lombok.Setter;

/**
 * @author: zhang
 * @date: 2020/11/8 18:13
 */
@Getter
@Setter
public class ThreadPoolConfig {
    private int corePoolSize = 5;
    private int keepAliveSeconds = 10;
    private int maximumPoolSize = 20;
    private int queueCapacity = 100;
    private String threadName = "myThreadPool";
}
