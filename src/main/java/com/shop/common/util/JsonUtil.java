package com.shop.common.util;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

/**
 * @author: Lin Song
 * @Date: 2020/4/22
 */
@Slf4j
public class JsonUtil {
    public static final ObjectMapper DEFAULT_OBJECT_MAPPER;
    public static final ObjectMapper OBJECT_MAPPER;
    private static final String EMPTY_JSON;

    static {
        LocalDateTimeDeserializer dateTimeDeserializer = new LocalDateTimeDeserializer(DateTimeUtil.DATE_TIME_FORMATTER);
        LocalDateTimeSerializer dateTimeSerializer = new LocalDateTimeSerializer(DateTimeUtil.DATE_TIME_FORMATTER);
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addDeserializer(LocalDateTime.class, dateTimeDeserializer);
        javaTimeModule.addSerializer(LocalDateTime.class, dateTimeSerializer);

        SimpleModule simpleModule = new SimpleModule();

        simpleModule.addDeserializer(String.class, new StringJsonDeserializer());
        OBJECT_MAPPER = new ObjectMapper()
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(javaTimeModule)
                .registerModule(simpleModule)
                .setDateFormat(new SimpleDateFormat())
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);

        DEFAULT_OBJECT_MAPPER = new ObjectMapper()
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(javaTimeModule)
                .registerModule(simpleModule)
                .setDateFormat(new SimpleDateFormat())
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
                .setSerializationInclusion(JsonInclude.Include.ALWAYS);

        String empty = StringUtils.EMPTY;
        try {
            empty = OBJECT_MAPPER.writeValueAsString(Collections.emptyMap());
        } catch (JsonProcessingException e) {
            log.warn("failed to init empty json", e);
        }
        EMPTY_JSON = empty;
    }

    private JsonUtil() {
    }

    public static <T> String toJson(T object) {
        if (object == null) {
            return EMPTY_JSON;
        }
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (Exception e) {
            log.warn("failed to convert to json", e);
            return EMPTY_JSON;
        }
    }

    public static <T> String toJsonContainsNull(T object) {
        if (object == null) {
            return EMPTY_JSON;
        }
        try {
            return DEFAULT_OBJECT_MAPPER.writeValueAsString(object);
        } catch (Exception e) {
            log.warn("failed to convert to json", e);
            return EMPTY_JSON;
        }
    }

    public static <T> Optional<T> parse(String json, Class<T> cls) {
        if (StringUtils.isBlank(json)) {
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(OBJECT_MAPPER.readValue(json, cls));
        } catch (Exception e) {
            log.warn("failed to parse json {}", json, e);
        }
        return Optional.empty();
    }


    static class StringJsonDeserializer extends JsonDeserializer<String> {
        @Override
        public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            return StringUtils.trimToNull(p.getValueAsString());
        }
    }
}
