package com.shop.common.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

/**
 * @author: Lin Song
 * @Date: 2020/5/18
 */
public class DateTimeUtil {
    public static final DateTimeFormatter DATE_TIME_FORMATTER =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    public static final DateTimeFormatter DUE_DATE_TIME_FORMATTER =
            DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private DateTimeUtil() {
    }

    public static LocalDateTime parse(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        return LocalDateTime.parse(str, DATE_TIME_FORMATTER);
    }

    public static LocalDateTime parseDueDate(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        return LocalDateTime.of(LocalDate.parse(str, DUE_DATE_TIME_FORMATTER), LocalTime.MAX);
    }

    public static long getDayLengthToDueDate(String dueDateStr) {
        return getDayLengthToDueDate(LocalDate.now(), dueDateStr);
    }

    public static long getDayLengthToDueDate(LocalDate now, String dueDateStr) {
        int day = now.getDayOfMonth();
        int dueDay = Integer.parseInt(dueDateStr);
        if (day == dueDay) {
            return 0;
        }
        LocalDate dueDate;
        if (dueDay < day) {
            dueDate = LocalDate.of(now.getYear(), now.getMonthValue(), dueDay);
        } else {
            LocalDate lastMonth = now.minusMonths(1);
            LocalDate lastDay = lastMonth.with(TemporalAdjusters.lastDayOfMonth());
            dueDay = Math.min(lastDay.getDayOfMonth(), dueDay);
            dueDate = LocalDate.of(lastMonth.getYear(), lastMonth.getMonth(), dueDay);
        }

        return dueDate.until(now, ChronoUnit.DAYS);
    }

    public static LocalDate getToleranceDate(String dueDay, String tolerance) {
        return getToleranceDate(null, dueDay, tolerance);
    }

    public static LocalDate getToleranceDate(LocalDate referenceDate, String dueDayStr, String toleranceStr) {
        LocalDate date = referenceDate != null ? referenceDate : LocalDate.now();
        LocalDate dueDate = LocalDate.of(date.getYear(), date.getMonth(), getDay(dueDayStr));
        return dueDate.plusDays(getDay(toleranceStr));
    }

    private static int getDay(String dayStr) {
        if (NumberUtils.isDigits(dayStr)) {
            return Integer.parseInt(dayStr);
        }
        return 1;
    }
}
