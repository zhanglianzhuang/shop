package com.shop.common.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author: zhang
 * @date: 2020/11/7 19:46
 */
public class OrderUtil {
    private static final String FORMATTER = "yyyyMMddHHmmss";

    public static String getOrderNo(int uid, int cid) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(FORMATTER);
        String format = dateTimeFormatter.format(LocalDateTime.now());
        return new StringBuffer().append(uid).append("-").append(format).append("-").append(cid).toString();
    }
}
