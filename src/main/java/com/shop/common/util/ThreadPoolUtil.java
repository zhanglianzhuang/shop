package com.shop.common.util;

import com.shop.common.config.ThreadPoolConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author: Lin Song
 * @Date: 2020/6/18
 */
@Slf4j
public class ThreadPoolUtil {
    private ThreadPoolUtil() {
    }

    public static ThreadPoolTaskExecutor createTaskExecutor(ThreadPoolConfig conf) {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(conf.getCorePoolSize());
        taskExecutor.setKeepAliveSeconds(conf.getKeepAliveSeconds());
        taskExecutor.setMaxPoolSize(conf.getMaximumPoolSize());
        taskExecutor.setQueueCapacity(conf.getQueueCapacity());
        taskExecutor.setThreadGroupName(conf.getThreadName());
        taskExecutor.setRejectedExecutionHandler((r, executor) ->
                log.warn("[ThreadConfiguration - " + conf.getThreadName() +
                        "] Task Queue is full! New task rejected!"));
        taskExecutor.initialize();
        return taskExecutor;
    }
}
