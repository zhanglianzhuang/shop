package com.shop.common.util;

import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
public class FileUtil {
    private FileUtil() {
    }

    public static byte[] zipTheReportWithBytes(byte[] bytesNeedZip, String reportName) {
        if (bytesNeedZip == null) {
            log.error("the fileNeedZip is null in BiReportSendEmailClientImpl for function zipTheReport");
            return new byte[0];
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (
                ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream)) {
            ZipEntry entry = new ZipEntry(reportName + ".xls");
            entry.setSize(bytesNeedZip.length);
            zipOutputStream.putNextEntry(entry);
            zipOutputStream.write(bytesNeedZip);
            zipOutputStream.closeEntry();
        } catch (Exception e) {
            log.warn(" zip file failed in AbstractSendEmail for function zipTheReport", e);
        }

        return byteArrayOutputStream.toByteArray();
    }

    public static String encodeBase64FileWithBytes(byte[] file) {
        if (file == null) {
            log.error("the file is null in BiReportSendEmailClientImpl for function encodeBase64File");
            return null;
        }
        return Base64.getEncoder().encodeToString(file);
    }
}
