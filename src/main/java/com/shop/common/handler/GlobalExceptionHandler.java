package com.shop.common.handler;

import com.shop.api.controller.*;
import com.shop.api.pojo.ErrorResponse;
import com.shop.core.exception.BaseException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: zhang
 * @date: 2020/11/12 20:18
 */
@ControllerAdvice(assignableTypes = {CommodityController.class, CommodityFavouriteController.class,
        CommodityTypeController.class, DirectDebitLogController.class, MessageController.class,
        PayController.class, ShoppingCarController.class, UserController.class})
@ResponseBody
public class GlobalExceptionHandler {
    @ExceptionHandler(BaseException.class)
    public ResponseEntity<?> handleAppException(BaseException ex, HttpServletRequest request) {
        ErrorResponse representation = new ErrorResponse(ex, request.getRequestURI());
        return new ResponseEntity<>(representation, new HttpHeaders(), ex.getExceptionEnum().getStatus());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleAllException(Exception ex) {
        return new ResponseEntity<>(ex, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
