package com.shop.core.exception;

import com.shop.core.enume.ExceptionEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseException extends RuntimeException {
    private final ExceptionEnum exceptionEnum;

    public BaseException(ExceptionEnum exceptionEnum) {
        super(exceptionEnum.toString());
        this.exceptionEnum = exceptionEnum;
    }
}
