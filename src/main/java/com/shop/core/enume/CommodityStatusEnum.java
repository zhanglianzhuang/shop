package com.shop.core.enume;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CommodityStatusEnum {
    Checking(1, "Checking"), ON_SALE(2, "on sale"), OFF_SALE(3, "off sale");
    private final int statusId;
    private final String statusName;
}
