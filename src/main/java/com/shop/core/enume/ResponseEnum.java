package com.shop.core.enume;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseEnum {
    SUCCESS(1, "success"), FAILED(0, "failed"),
    PHONE_NUMBER_INVALID(40001, "the phone number is invalid");


    private final int code;
    private final String message;
}
