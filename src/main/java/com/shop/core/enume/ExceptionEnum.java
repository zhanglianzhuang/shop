package com.shop.core.enume;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ExceptionEnum {
    INSERT_COMMODITY_ERROR(40001, HttpStatus.BAD_REQUEST, "insert commodity error"),
    INSERT_FAVOURITE_ERROR(40001, HttpStatus.BAD_REQUEST, "insert favourite commodity error");
    private final int code;
    private final HttpStatus status;
    private final String message;
}
