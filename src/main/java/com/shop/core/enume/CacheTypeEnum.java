package com.shop.core.enume;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Duration;

@Getter
@AllArgsConstructor
public enum CacheTypeEnum {
    DEFAULT("default", Duration.ofMinutes(30)), USER("user", Duration.ofMinutes(30)),
    COMMODITY_TYPE("type", Duration.ofDays(30)), COMMODITY_STATUS("status", Duration.ofDays(1)),
    SHOPPING_CAR("car", Duration.ofMinutes(30)), SHOPPING_FAVORITE("favor", Duration.ofMinutes(30));
    private final String prefix;
    private final Duration ttl;
}
