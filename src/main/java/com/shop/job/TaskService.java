package com.shop.job;

import com.shop.persist.entity.Commodity;
import com.shop.persist.mapper.CommodityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: zhang
 * @date: 2020/11/14 21:16
 */
@Service
public class TaskService {
    @Autowired
    private CommodityMapper commodityMapper;

    public Commodity getCommodity(int cid) {
        return commodityMapper.selectCommodityById(cid);

    }

}
