package com.shop.job;

import com.shop.common.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class MessageTask {
    @Autowired
    private MessageService messageService;

    @Scheduled(cron = "0 0 0 * * ?")
    public void saveMessage() {
        messageService.saveMessage(LocalDate.now().minusDays(1));
    }
}
