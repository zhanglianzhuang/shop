package com.shop.job;

import com.shop.api.service.DirectDebitLogService;
import com.shop.api.service.Impl.PayService;
import com.shop.api.service.UserService;
import com.shop.persist.entity.DirectDebitLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: zhang
 * @date: 2020/11/14 20:10
 */
@Component
@Slf4j
public class RevenueShareTask {
    @Autowired
    private DirectDebitLogService directDebitLogService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private UserService userService;
    @Autowired
    private PayService payService;

    @Scheduled(cron = "0 0 0 1 * ?")
    public void share() {
        LocalDateTime now = LocalDateTime.now();
        List<DirectDebitLog> directDebitLogList = directDebitLogService.getDirectDebitLogsByTime(now.minusMonths(1), now);
        if (directDebitLogList.isEmpty()) {
            log.info("no revenue need to share");
            return;
        }
        Map<Integer, List<DirectDebitLog>> collect = directDebitLogList.stream()
                .filter(directDebitLog -> "TRADE_SUCCESS".equals(directDebitLog.getPhases()))
                .collect(Collectors.groupingBy(DirectDebitLog::getCid));
        collect.forEach((K, V) -> {
            int creatorId = taskService.getCommodity(K).getCreator();
            String alipayId = userService.getUser(creatorId).getAliPayId();
            double sum = V.stream().mapToDouble(DirectDebitLog::getPrice).sum() * 0.7;
            userService.updateDesignerIncome(K, sum);
            payService.revenueShare(alipayId, sum);
        });

    }
}
