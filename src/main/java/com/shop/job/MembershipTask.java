package com.shop.job;

import com.shop.api.service.MembershipService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author: zhang
 * @date: 2020/11/14 16:33
 */
@Component
@Slf4j
public class MembershipTask {
    @Autowired
    private MembershipService membershipService;

    @Scheduled(cron = "0 0 0 * * ?")
    public void updateMembershipCounts() {
        int result = membershipService.updateAllMembership();
        log.info("更新{}个会员每日免费下载量", result);
    }
}
