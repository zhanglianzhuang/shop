package com.shop.listener.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: zhang
 * @date: 2020/11/7 22:30
 */
@Getter
@AllArgsConstructor
public class DirectDebitLogEvent {
    private final int uid;
    private final int cid;
    private final boolean clear;
    private final String orderId;
    private final String phases;
}
