package com.shop.listener.listener;

import com.shop.api.service.DirectDebitLogService;
import com.shop.api.service.ShoppingCarService;
import com.shop.listener.event.DirectDebitLogEvent;
import com.shop.persist.entity.DirectDebitLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author: zhang
 * @date: 2020/11/7 22:37
 */
@Component
public class DirectDebitListener {
    @Autowired
    private DirectDebitLogService directDebitLogService;
    @Autowired
    private ShoppingCarService shoppingCarService;

    @Async
    @EventListener
    public void saveDirectDebitLog(DirectDebitLogEvent directDebitLogEvent) {
        DirectDebitLog directDebitLog = new DirectDebitLog();
        directDebitLog.setUid(directDebitLogEvent.getUid());
        directDebitLog.setCid(directDebitLogEvent.getCid());
        directDebitLog.setPhases(directDebitLogEvent.getPhases());
        directDebitLog.setOrderId(directDebitLogEvent.getOrderId());
        directDebitLogService.addDirectDebitLog(directDebitLog);
        if (directDebitLogEvent.isClear()) {
            shoppingCarService.removeCommodity(directDebitLog.getUid(), directDebitLog.getCid());
        }
    }
}
