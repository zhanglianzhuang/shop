package com.shop.listener.listener;

import com.shop.api.pojo.MessageDto;
import com.shop.common.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class MessageListener {
    @Autowired
    private MessageService messageService;

    @Async
    @EventListener
    public void saveMessage(MessageDto messageDto) {
        messageService.saveMessage(messageDto, LocalDate.now());
    }
}
