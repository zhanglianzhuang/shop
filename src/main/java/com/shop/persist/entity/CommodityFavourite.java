package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommodityFavourite {
    private int id;
    private int uid;
    private int cid;
    private LocalDateTime CDate;
}
