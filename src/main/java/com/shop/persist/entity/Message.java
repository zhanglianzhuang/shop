package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Message {
    private int id;
    private int fromUid;
    private int toUid;
    private String content;
    private LocalDateTime CDATE;
    private boolean reading;
}
