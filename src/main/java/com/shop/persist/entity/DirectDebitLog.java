package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
public class DirectDebitLog {
    private int id;
    private int uid;
    private int cid;
    private double price;
    private String orderId;
    private String aliPayId;
    private String buyerId;
    private String phases;
    private LocalDateTime CDate;
    private LocalDateTime UDate;
}
