package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ShoppingCar {
    private int id;
    private int uid;
    private int cid;
    private int num;
    private LocalDateTime CDate;
}
