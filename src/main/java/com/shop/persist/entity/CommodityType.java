package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommodityType {
    private int id;
    private String typeName;
}
