package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Commodity {
    private int id;
    private String commodityName;
    private double price;
    private int typeId;
    private int statusId;
    private LocalDateTime CDate;
    private LocalDateTime UDate;
    private String editor;
    private int creator;
    private String introduction;
    private String title;
    private String picPath;
}
