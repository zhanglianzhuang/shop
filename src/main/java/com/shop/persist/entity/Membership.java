package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Membership {
    private int id;
    private int uid;
    private int counts;
    private LocalDateTime CDATE;
}
