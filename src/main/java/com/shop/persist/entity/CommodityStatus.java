package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommodityStatus {
    private int id;
    private String statusName;
}
