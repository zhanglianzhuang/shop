package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Role {
    private int id;
    private String roleName;
}
