package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author: zhang
 * @date: 2020/11/11 20:48
 */
@Getter
@Setter
public class UserBuying {
    private int id;
    private int uid;
    private int cid;
    private LocalDateTime CDate;
}
