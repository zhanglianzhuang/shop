package com.shop.persist.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class User {
    private int id;
    private String username;
    private String password;
    private String phoneNo;
    private int roleId;
    private boolean membership;
    private String picUrl;
    private LocalDate createDate;
    private String aliPayId;
    private String idCard;
    private String trueName;
    private boolean logout;
    private double membershipIncome;
    private double designerIncome;
}
