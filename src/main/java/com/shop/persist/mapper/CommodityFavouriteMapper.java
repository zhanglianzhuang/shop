package com.shop.persist.mapper;

import com.shop.persist.entity.CommodityFavourite;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommodityFavouriteMapper {
    int insertFavourite(int uid, int cid);

    List<CommodityFavourite> selectFavourite(int uid);

    int deleteFavourite(int uid, int cid);
}
