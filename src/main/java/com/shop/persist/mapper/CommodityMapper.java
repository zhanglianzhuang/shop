package com.shop.persist.mapper;

import com.shop.persist.entity.Commodity;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CommodityMapper {
    int insertCommodity(Commodity commodity);

    List<Commodity> selectCommodity(int start, int size, int statusId);

    Commodity selectCommodityById(int cid);

    List<Commodity> selectCommodityByTypeIdAndStatusId(int typeId, int statusId);

    int updateCommodity(int cid, int statusId, LocalDateTime UDate);

    List<Commodity> selectCommodityListByCreator(int uid);

    int updatePicPath(int cid, String picPath);

    List<Commodity> selectCommodityListByParam(String param);

    int getSize(int statusId);
}
