package com.shop.persist.mapper;

import com.shop.persist.entity.Membership;
import org.springframework.stereotype.Repository;

@Repository
public interface MembershipMapper {
    int insertMembership(int uid, int counts);

    Membership selectMembership(int uid);

    int updateMembership(int uid, int counts);

    int updateAllMembershipCount();
}
