package com.shop.persist.mapper;

import com.shop.persist.entity.CommodityType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommodityTypeMapper {
    List<CommodityType> selectAllTypes();

    CommodityType selectCommodityByTypeId(int typeId);

    CommodityType selectCommodityByTypeName(String typeName);
}
