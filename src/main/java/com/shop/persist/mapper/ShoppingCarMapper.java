package com.shop.persist.mapper;

import com.shop.persist.entity.ShoppingCar;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShoppingCarMapper {
    int insertCar(int uid, int cid);

    List<ShoppingCar> selectCar(int uid);

    int deleteCommodity(int uid, int cid);

    int reduceCommodity(int uid, int cid, int num);

    int deleteAllCommodities(int uid);
}
