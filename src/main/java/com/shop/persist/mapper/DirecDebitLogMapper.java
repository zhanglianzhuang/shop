package com.shop.persist.mapper;

import com.shop.persist.entity.DirectDebitLog;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface DirecDebitLogMapper {
    int insertDirecDebitLog(DirectDebitLog directDebitLog);

    int updateDirecDebitLog(DirectDebitLog directDebitLog);

    List<DirectDebitLog> selectDirecDebitLogs(int uid);

    DirectDebitLog selectDirecDebitLog(String orderId);

    List<DirectDebitLog> selectDirecDebitLogsByTime(LocalDateTime from, LocalDateTime to);
}
