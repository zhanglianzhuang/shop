package com.shop.persist.mapper;

import com.shop.persist.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zlz
 * @date 2020.9.24
 */
@Repository
public interface UserMapper {
    User selectUser(int id);

    int insertUser(User user);

    User selectUserByPhone(String phoneNumber);

    int updateUserRole(int uid, int roleId, String aliPayId);

    int updateVIP(int uid);

    int updatePassword(String phoneNumber, String password);

    int updateInfo(User user);

    int logout(int id);

    List<User> getAllUser();

    int updateMemberIncome(int id, double income);

    int updateDesignerIncome(int id, double income);

    List<User> getAdmin();
}
