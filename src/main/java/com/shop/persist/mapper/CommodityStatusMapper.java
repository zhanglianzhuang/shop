package com.shop.persist.mapper;

import com.shop.persist.entity.CommodityStatus;
import org.springframework.stereotype.Repository;

@Repository
public interface CommodityStatusMapper {
    CommodityStatus selectByName(String name);

    CommodityStatus selectById(int id);

}
