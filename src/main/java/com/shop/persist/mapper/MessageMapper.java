package com.shop.persist.mapper;

import com.shop.persist.entity.Message;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageMapper {
    int addMessage(Message message);

    List<Message> getMessage(int uid);

    int updateMessageForReading(int id);

    int selectUnReadingNum(int uid);
}
