package com.shop.persist.mapper;

import com.shop.persist.entity.UserBuying;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author: zhang
 * @date: 2020/11/11 20:51
 */
@Repository
public interface UserBuyingMapper {
    int add(int uid, int cid);

    List<UserBuying> select(int uid);
}
