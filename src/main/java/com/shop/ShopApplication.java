package com.shop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

@SpringBootApplication
@MapperScan(basePackages = {"com.shop.persist"})
@EnableWebSocket
@EnableCaching
@EnableAsync
@EnableScheduling
public class ShopApplication {
    //todo  1.上传页面* 2. 5个免费* 3.收益分成  4.支付集成  5.全局异常返回* 6.已购买商品* 7.ddlog更新* 8.reading* 9.搜索*
    public static void main(String[] args) {
        SpringApplication.run(ShopApplication.class, args);
    }

}
