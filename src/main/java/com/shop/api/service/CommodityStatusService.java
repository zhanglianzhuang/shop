package com.shop.api.service;

import com.shop.persist.entity.CommodityStatus;

public interface CommodityStatusService {
    CommodityStatus getStatusByName(String name);

    CommodityStatus getStatusById(int id);
}
