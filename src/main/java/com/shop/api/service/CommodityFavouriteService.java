package com.shop.api.service;

import com.shop.api.pojo.CommodityDto;

import java.util.List;

public interface CommodityFavouriteService {
    int addFavourite(int uid, int cid);

    List<CommodityDto> getFavourite(int uid);

    int removeFavourite(int uid, int cid);
}
