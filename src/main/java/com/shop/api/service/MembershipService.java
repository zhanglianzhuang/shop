package com.shop.api.service;

import com.shop.persist.entity.Membership;

/**
 * @author: zhang
 * @date: 2020/11/10 22:18
 */
public interface MembershipService {
    int addMembership(int uid, int counts);

    Membership getMembership(int uid);

    int updateMembership(int uid, int counts);

    int updateAllMembership();
}
