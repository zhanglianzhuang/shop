package com.shop.api.service;

import com.shop.api.entity.Authentication;
import com.shop.api.pojo.UserDto;
import com.shop.persist.entity.User;

import java.util.List;
import java.util.Optional;


public interface UserService {
    User getUser(int id);

    int insertUser(User user);

    Optional<UserDto> getUserCheck(Authentication authentication);

    int register(Authentication authentication);

    int updateUser(int uid, int roleId, String aliPayId);

    int updateVIP(int uid);

    int updatePassword(Authentication authentication);

    int updateInfo(UserDto userDto);

    int logout(int id);

    List<UserDto> getUserList();

    int updateMemberIncome(int id, double income);

    int updateDesignerIncome(int id, double income);

    List<UserDto> getAdminList();
}
