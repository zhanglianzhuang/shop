package com.shop.api.service.Impl;

import com.shop.api.pojo.CommodityDto;
import com.shop.api.service.CommodityFavouriteService;
import com.shop.api.service.CommodityService;
import com.shop.core.enume.ExceptionEnum;
import com.shop.core.exception.BaseException;
import com.shop.persist.entity.CommodityFavourite;
import com.shop.persist.mapper.CommodityFavouriteMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CommodityFavouriteServiceImpl implements CommodityFavouriteService {
    @Autowired
    private CommodityFavouriteMapper commodityFavouriteMapper;
    @Autowired
    private CommodityService commodityService;

    @Override
    @CacheEvict(value = "favor", key = "'user:'+#p0")
    public int addFavourite(int uid, int cid) {
        int result = commodityFavouriteMapper.insertFavourite(uid, cid);
        if (result != 1) {
            log.warn("insert favourite commodity error");
            throw new BaseException(ExceptionEnum.INSERT_FAVOURITE_ERROR);
        }
        return result;
    }

    @Override
    @Cacheable(value = "favor", key = "'user:'+#p0")
    public List<CommodityDto> getFavourite(int uid) {
        List<CommodityFavourite> commodityFavourites = commodityFavouriteMapper.selectFavourite(uid);
        if (commodityFavourites.isEmpty()) {
            return Collections.emptyList();
        }
        return commodityFavourites.stream()
                .map(t -> commodityService.getCommodity(t.getCid())).collect(Collectors.toList());
    }

    @Override
    @CacheEvict(value = "favor", key = "'user:'+#p0")
    public int removeFavourite(int uid, int cid) {
        return commodityFavouriteMapper.deleteFavourite(uid, cid);
    }
}
