package com.shop.api.service.Impl;

import com.shop.api.pojo.CommodityDto;
import com.shop.api.service.CommodityService;
import com.shop.api.service.UserBuyingService;
import com.shop.persist.entity.UserBuying;
import com.shop.persist.mapper.UserBuyingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: zhang
 * @date: 2020/11/11 20:55
 */
@Service
public class UserBuyingServiceImpl implements UserBuyingService {
    @Autowired
    private UserBuyingMapper userBuyingMapper;
    @Autowired
    private CommodityService commodityService;

    @Override
    public int addUserBuying(int uid, int cid) {
        return userBuyingMapper.add(uid, cid);
    }

    @Override
    public List<CommodityDto> getUserBuying(int uid) {
        List<UserBuying> select = userBuyingMapper.select(uid);
        return select.stream()
                .map(t -> commodityService.getCommodity(t.getCid()))
                .collect(Collectors.toList());
    }
}
