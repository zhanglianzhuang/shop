package com.shop.api.service.Impl;

import com.shop.api.service.CommodityTypeService;
import com.shop.persist.entity.CommodityType;
import com.shop.persist.mapper.CommodityTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommodityTypeServiceImpl implements CommodityTypeService {
    @Autowired
    private CommodityTypeMapper commodityTypeMapper;


    @Override
    @Cacheable(value = "type", key = "'list:'")
    public List<CommodityType> getAllTypes() {
        return commodityTypeMapper.selectAllTypes();
    }

    @Override
    @Cacheable(value = "type", key = "'typeId:'+#p0")
    public CommodityType getCommodityTypeByTypeId(int typeId) {
        return commodityTypeMapper.selectCommodityByTypeId(typeId);
    }

    @Override
    @Cacheable(value = "type", key = "'typeName:'+#p0")
    public CommodityType getCommodityTypeByTypeName(String typeName) {
        return commodityTypeMapper.selectCommodityByTypeName(typeName);
    }
}
