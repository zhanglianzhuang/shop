package com.shop.api.service.Impl;

import com.shop.api.service.MembershipService;
import com.shop.persist.entity.Membership;
import com.shop.persist.mapper.MembershipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: zhang
 * @date: 2020/11/10 22:18
 */
@Service
public class MembershipServiceImpl implements MembershipService {
    @Autowired
    private MembershipMapper membershipMapper;

    @Override
    public int addMembership(int uid, int counts) {
        return membershipMapper.insertMembership(uid, counts);
    }

    @Override
    public Membership getMembership(int uid) {
        return membershipMapper.selectMembership(uid);
    }

    @Override
    public int updateMembership(int uid, int counts) {
        return membershipMapper.updateMembership(uid, counts);
    }

    @Override
    public int updateAllMembership() {
        return membershipMapper.updateAllMembershipCount();
    }
}
