package com.shop.api.service.Impl;

import com.shop.api.entity.Authentication;
import com.shop.api.pojo.UserDto;
import com.shop.api.service.UserService;
import com.shop.persist.entity.User;
import com.shop.persist.mapper.UserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    @Cacheable(value = "user", key = "'id:'+#p0")
    public User getUser(int id) {
        return userMapper.selectUser(id);
    }

    @Override
    public int insertUser(User user) {
        return userMapper.insertUser(user);
    }

    @Override
    @Cacheable(value = "user", key = "'phone:'+#p0.phoneNumber", unless = "#result==null")
    public Optional<UserDto> getUserCheck(Authentication authentication) {
        User user = userMapper.selectUserByPhone(authentication.getPhoneNumber());
        if (Objects.isNull(user)) {
            return Optional.empty();
        }
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(user, userDto);
        if (user.getPassword().equals(authentication.getPassword())) {
            return Optional.of(userDto);
        }
        return Optional.empty();
    }

    @Override
    public int register(Authentication authentication) {
        return userMapper.insertUser(buildUser(authentication));
    }

    @Override
    @CacheEvict(value = "user", key = "'id:'+#p0")
    public int updateUser(int uid, int roleId, String aliPayId) {
        return userMapper.updateUserRole(uid, roleId, aliPayId);
    }

    @Override
    @CacheEvict(value = "user", key = "'id:'+#p0")
    public int updateVIP(int uid) {
        return userMapper.updateVIP(uid);
    }

    @Override
    @CacheEvict(value = "user", key = "'phone:'+#p0.phoneNumber")
    public int updatePassword(Authentication authentication) {
        return userMapper.updatePassword(authentication.getPhoneNumber(), authentication.getPassword());
    }

    @Override
    @CacheEvict(value = "user", key = "'id:'+#p0.id")
    public int updateInfo(UserDto userDto) {
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        return userMapper.updateInfo(user);
    }

    @Override
    public int logout(int id) {
        return userMapper.logout(id);
    }

    @Override
    public List<UserDto> getUserList() {
        List<User> allUser = userMapper.getAllUser();
        return allUser.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    public List<UserDto> getAdminList() {
        List<User> allUser = userMapper.getAdmin();
        return allUser.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    @CacheEvict(value = "user", key = "'id:'+#p0")
    public int updateMemberIncome(int id, double income) {
        return userMapper.updateMemberIncome(id, income);
    }

    @Override
    @CacheEvict(value = "user", key = "'id:'+#p0")
    public int updateDesignerIncome(int id, double income) {
        return userMapper.updateDesignerIncome(id, income);
    }

    private User buildUser(Authentication authentication) {
        User user = new User();
        user.setPhoneNo(authentication.getPhoneNumber());
        user.setUsername("shop_" + authentication.getPhoneNumber());
        user.setPassword(authentication.getPassword());
        user.setPicUrl("path");
        user.setMembership(false);
        user.setRoleId(1);
        return user;
    }

    private UserDto convert(User user) {
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(user, userDto);
        return userDto;
    }
}
