package com.shop.api.service.Impl;

import com.shop.api.service.CommodityStatusService;
import com.shop.persist.entity.CommodityStatus;
import com.shop.persist.mapper.CommodityStatusMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class CommodityStatusServiceImpl implements CommodityStatusService {
    @Autowired
    private CommodityStatusMapper commodityStatusMapper;

    @Override
    @Cacheable(value = "status", key = "'name'+#p0")
    public CommodityStatus getStatusByName(String name) {
        return commodityStatusMapper.selectByName(name);
    }

    @Override
    @Cacheable(value = "status", key = "'id'+#p0")
    public CommodityStatus getStatusById(int id) {
        return commodityStatusMapper.selectById(id);
    }
}
