package com.shop.api.service.Impl;

import com.shop.api.pojo.CommodityDto;
import com.shop.api.service.CommodityService;
import com.shop.api.service.ShoppingCarService;
import com.shop.persist.entity.ShoppingCar;
import com.shop.persist.mapper.ShoppingCarMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShoppingCarServiceImpl implements ShoppingCarService {
    @Autowired
    private ShoppingCarMapper shoppingCarMapper;

    @Autowired
    private CommodityService commodityService;

    @Override
    @CacheEvict(value = "car", key = "'user:'+#p0")
    public int addCar(int uid, int cid) {
        return shoppingCarMapper.insertCar(uid, cid);
    }

    @Override
    @Cacheable(value = "car", key = "'user:'+#p0")
    public List<CommodityDto> getCar(int uid) {
        List<ShoppingCar> shoppingCars = shoppingCarMapper.selectCar(uid);
        return shoppingCars.stream().map(t -> commodityService.getCommodity(t.getCid())).collect(Collectors.toList());
    }

    @Override
    @CacheEvict(value = "car", key = "'user:'+#p0")
    public int removeCommodity(int uid, int cid) {
        return shoppingCarMapper.deleteCommodity(uid, cid);
    }

    @Override
    @CacheEvict(value = "car", key = "'user:'+#p0")
    public int reduceCommodity(int uid, int cid, int num) {
        return shoppingCarMapper.reduceCommodity(uid, cid, num);
    }

    @Override
    @CacheEvict(value = "car", key = "'user:'+#p0")
    public int removeAllCommodities(int uid) {
        return shoppingCarMapper.deleteAllCommodities(uid);
    }
}
