package com.shop.api.service.Impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.util.ZipUtil;
import com.shop.api.pojo.CommodityDto;
import com.shop.api.pojo.UploadDto;
import com.shop.api.service.CommodityService;
import com.shop.api.service.CommodityStatusService;
import com.shop.api.service.CommodityTypeService;
import com.shop.api.service.UserService;
import com.shop.core.enume.CommodityStatusEnum;
import com.shop.core.enume.ExceptionEnum;
import com.shop.core.exception.BaseException;
import com.shop.persist.entity.Commodity;
import com.shop.persist.entity.CommodityStatus;
import com.shop.persist.entity.CommodityType;
import com.shop.persist.mapper.CommodityMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CommodityServiceImpl implements CommodityService {
    @Autowired
    private CommodityMapper commodityMapper;
    @Autowired
    private CommodityTypeService commodityTypeService;
    @Autowired
    private CommodityStatusService commodityStatusService;
    @Autowired
    private UserService userService;

    private final String FILE_PATH = "/work/java_workspace/shop/file";
    private final String SEP = File.separator;

    @Override
    public int addCommodity(UploadDto uploadDto) {
        Commodity commodity = new Commodity();
        commodity.setCommodityName(uploadDto.getCommodityName());
        commodity.setTitle(uploadDto.getTitle());
        commodity.setIntroduction(uploadDto.getIntroduction());
        commodity.setCreator(uploadDto.getCreator());
        commodity.setPrice(uploadDto.getPrice());
        commodity.setTypeId(1);
        commodity.setStatusId(1);
        commodity.setCDate(LocalDateTime.now());
        commodity.setUDate(LocalDateTime.now());
        commodity.setStatusId(CommodityStatusEnum.Checking.getStatusId());
        int result = commodityMapper.insertCommodity(commodity);
        saveFile(uploadDto.getFile(), commodity.getId());
        if (result != 1) {
            log.error("insert commodity failed [{}]", commodity);
            throw new BaseException(ExceptionEnum.INSERT_COMMODITY_ERROR);
        }
        return result;
    }

    @Override
    public List<CommodityDto> getCommodityList(int start, int size, int statusId) {
        List<Commodity> commodities = commodityMapper.selectCommodity(start, size, statusId);
        return commodities.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    public CommodityDto getCommodity(int cid) {
        Commodity commodity = commodityMapper.selectCommodityById(cid);
        return convert(commodity);
    }

    @Override
    public List<CommodityDto> getCommoditiesByTypeAndStatus(String typeName, String statusName) {
        CommodityType commodityType = commodityTypeService.getCommodityTypeByTypeName(typeName);
        CommodityStatus commodityStatus = commodityStatusService.getStatusByName(statusName);
        List<Commodity> commodities = commodityMapper.selectCommodityByTypeIdAndStatusId(commodityType.getId(), commodityStatus.getId());
        return commodities.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    public int updateCommodity(int cid, int statusId) {
        return commodityMapper.updateCommodity(cid, statusId, LocalDateTime.now());
    }

    @Override
    public List<CommodityDto> getCommodityListByUserId(int uid) {
        List<Commodity> commodities = commodityMapper.selectCommodityListByCreator(uid);
        return commodities.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    public String download(int cid, HttpServletResponse response) {
        String filename = UUID.randomUUID().toString() + ".zip";
        File path = new File(FILE_PATH + SEP + "zip" + SEP + cid + ".zip");
        if (path.exists()) { //判断文件父目录是否存在
            response.setContentType("application/zip;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            try {
                response.setHeader("Content-Disposition", "attachment;fileName=" + java.net.URLEncoder.encode(filename, "UTF-8"));
                byte[] buffer = new byte[1024];
                FileInputStream fis; //文件输入流
                BufferedInputStream bis;
                OutputStream os; //输出流
                os = response.getOutputStream();
                fis = new FileInputStream(path);
                bis = new BufferedInputStream(fis);
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer);
                    i = bis.read(buffer);
                }

                log.info("----------file download---cid:{}", cid);

                bis.close();
                fis.close();
            } catch (IOException e) {
                log.error("下载失败!! cid:{} exception:{}", cid, e);
            }
        }

        return null;
    }

    @Override
    public List<CommodityDto> searchCommodities(String param) {
        List<Commodity> commodities = commodityMapper.selectCommodityListByParam(param);
        return commodities.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    public int getSize(int statusId) {
        return commodityMapper.getSize(statusId);
    }

    private void saveFile(MultipartFile[] files, int cid) {
        String path = new StringBuilder()
                .append(FILE_PATH)
                .append(SEP)
                .append(cid)
                .append(SEP)
                .toString();
        log.info(path);
        for (MultipartFile file : files) {
            File folderFile = new File(path);
            folderFile.mkdirs();
            File save = new File(path + file.getOriginalFilename());
            if (Objects.nonNull(file.getContentType()) && file.getContentType().contains("image")) {
                commodityMapper.updatePicPath(cid, path + file.getOriginalFilename());
            }
            try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(save))) {
                outputStream.write(file.getBytes());
                outputStream.flush();
            } catch (IOException e) {
                log.error("保存文件失败,cid:{},ex:{}", cid, e.getStackTrace());
            }
        }
        log.info("将ppt 打包 cid:{}", cid);
        ZipUtil.zip(path, FILE_PATH + SEP + "zip" + SEP + cid + ".zip");
    }

    private CommodityDto convert(Commodity commodity) {
        CommodityDto commodityDto = new CommodityDto();
        commodityDto.setCDate(commodity.getCDate());
        commodityDto.setCommodityName(commodity.getCommodityName());
        commodityDto.setIntroduction(commodity.getIntroduction());
        commodityDto.setPrice(commodity.getPrice());
        commodityDto.setTitle(commodity.getTitle());
        commodityDto.setStatusId(commodity.getStatusId());
        commodityDto.setId(commodity.getId());
        commodityDto.setTypeId(commodity.getTypeId());
        commodityDto.setUDate(commodity.getUDate());
        commodityDto.setCreator(userService.getUser(commodity.getCreator()).getUsername());
        File file = new File(commodity.getPicPath());
        ImgUtil.scale(file, file, 640, 400, Color.WHITE);
        commodityDto.setPic(Base64.encode(file));
        commodityDto.setCreatorId(commodity.getCreator());
        return commodityDto;
    }
}
