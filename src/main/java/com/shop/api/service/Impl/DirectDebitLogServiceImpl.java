package com.shop.api.service.Impl;

import com.shop.api.service.DirectDebitLogService;
import com.shop.persist.entity.DirectDebitLog;
import com.shop.persist.mapper.DirecDebitLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: zhang
 * @date: 2020/11/7 22:39
 */
@Service
public class DirectDebitLogServiceImpl implements DirectDebitLogService {
    @Autowired
    private DirecDebitLogMapper direcDebitLogMapper;

    @Override
    public int addDirectDebitLog(DirectDebitLog directDebitLog) {
        return direcDebitLogMapper.insertDirecDebitLog(directDebitLog);
    }

    @Override
    public int updateDirectDebitLog(DirectDebitLog directDebitLog) {
        return direcDebitLogMapper.updateDirecDebitLog(directDebitLog);
    }

    @Override
    public List<DirectDebitLog> getDirectDebitLogs(int uid) {
        return direcDebitLogMapper.selectDirecDebitLogs(uid);
    }

    @Override
    public DirectDebitLog getDirectDebitLog(String orderId) {
        return direcDebitLogMapper.selectDirecDebitLog(orderId);
    }

    @Override
    public List<DirectDebitLog> getDirectDebitLogsByTime(LocalDateTime from, LocalDateTime to) {
        return direcDebitLogMapper.selectDirecDebitLogsByTime(from, to);
    }
}
