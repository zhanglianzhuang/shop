package com.shop.api.service.Impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayFundTransUniTransferRequest;
import com.alipay.api.response.AlipayFundTransUniTransferResponse;
import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse;
import com.shop.api.service.CommodityService;
import com.shop.common.util.OrderUtil;
import com.shop.listener.event.DirectDebitLogEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: zhang
 * @date: 2020/11/7 19:43
 */
@Service
@Slf4j
public class PayService {
    @Value("${alipay.appId}")
    private String appId;

    //私钥
    @Value("${alipay.privateKey}")
    private String privateKey;

    //公钥
    @Value("${alipay.publicKey}")
    private String publicKey;
    //支付成功后要跳转的页面
    @Value("${alipay.returnUrl}")
    private String returnUrl;
    private static final String SPLIT = ",";
    private static final String PHASES = "create";
    @Autowired
    private CommodityService commodityService;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    /**
     * 下单支付
     */
    public String pay(int uid, String cid, boolean clear) {
        String[] cidStr = cid.split(SPLIT);
        BigDecimal totalPrice = new BigDecimal("0");
        String orderId = Strings.EMPTY;
        for (String i : cidStr) {
            int cidInteger = Integer.parseInt(i);
            orderId = OrderUtil.getOrderNo(uid, cidInteger);
            totalPrice = totalPrice.add(BigDecimal.valueOf(commodityService.getCommodity(cidInteger).getPrice()));
            applicationEventPublisher.publishEvent(new DirectDebitLogEvent(uid, cidInteger, clear, orderId, PHASES));
        }

        //调用sdk，发起支付
        AlipayTradePagePayResponse response;
        try {
            log.info("uid:{} 发起支付", uid);
            response = Factory.Payment
                    //选择网页支付平台
                    .Page()
                    //调用支付方法，设置订单名称、我们自己系统中的订单号、金额、回调页面
                    .pay("虚拟商品", orderId, totalPrice.toString(), returnUrl);
        } catch (Exception e) {
            log.error("aliPay cause exception {}", e.getMessage());
            return Strings.EMPTY;
        }

        //这里的response.body，就是一个可以直接加载的html片段，
        // 这里为了简单我直接返回这个片段，前端直接
        return response.body;
    }

    public String payVIP(int uid) {
        BigDecimal totalPrice = new BigDecimal("299");
        String orderId = "VIP/" + uid;
        AlipayTradePagePayResponse response;
        try {
            log.info("uid:{} 发起VIP支付", uid);
            response = Factory.Payment
                    .Page()
                    .pay("VIP", orderId, totalPrice.toString(), returnUrl);
        } catch (Exception e) {
            log.error("aliPay cause exception {}", e.getMessage());
            return Strings.EMPTY;
        }
        return response.body;
    }

    public void revenueShare(String alipayId, Double price) {

        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipaydev.com/gateway.do", appId, privateKey, "JSON", "UTF-8", publicKey, "RSA2");
        AlipayFundTransUniTransferRequest request = new AlipayFundTransUniTransferRequest();
        request.setBizContent("{" +
                "\"out_biz_no\":\"111111111111\"," +
                "\"product_code\":\"TRANS_ACCOUNT_NO_PWD\"," +
                "\"trans_amount\":\"" + price + "\"," +
                "\"biz_scene\":\"DIRECT_TRANSFER\"," +
//                "\"payer_info\":{" +
//                "\"identity\":\"imoafw2577@sandbox.com\"," +
//                "\"identity_type\":\"ALIPAY_USER_ID\"," +
//                "      }," +
                "\"payee_info\":{" +
                "\"identity\":\"idrtin2421@sandbox.com\"," +
                "\"identity_type\":\"ALIPAY_LOGON_ID\"," +
                "\"name\":\"idrtin2421\"" +
                "    }," +
                "\"remark\":\"收益分成\"," +
                "  }");
        AlipayFundTransUniTransferResponse response = null;
        Map<String, Object> map = new HashMap<>();
        try {
            log.info("转账发送---,参数为:{}", request.getBizContent());
            response = alipayClient.execute(request);
            if (response.isSuccess()) {
                log.info("调用成功");
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            map.put("success", "false");
            map.put("des", "转账失败！");
        }
        log.info(String.valueOf(map));

    }
}
