package com.shop.api.service;

import com.shop.persist.entity.DirectDebitLog;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: zhang
 * @date: 2020/11/7 22:38
 */
public interface DirectDebitLogService {
    int addDirectDebitLog(DirectDebitLog directDebitLog);

    int updateDirectDebitLog(DirectDebitLog directDebitLog);

    List<DirectDebitLog> getDirectDebitLogs(int uid);

    DirectDebitLog getDirectDebitLog(String orderId);

    List<DirectDebitLog> getDirectDebitLogsByTime(LocalDateTime from, LocalDateTime to);
}
