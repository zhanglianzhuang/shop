package com.shop.api.service;

import com.shop.api.pojo.CommodityDto;

import java.util.List;

/**
 * @author: zhang
 * @date: 2020/11/11 20:55
 */
public interface UserBuyingService {
    int addUserBuying(int uid, int cid);

    List<CommodityDto> getUserBuying(int uid);
}
