package com.shop.api.service;

import com.shop.api.pojo.CommodityDto;
import com.shop.api.pojo.UploadDto;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface CommodityService {
    int addCommodity(UploadDto uploadDto);

    List<CommodityDto> getCommodityList(int start, int size, int statusId);

    CommodityDto getCommodity(int cid);

    List<CommodityDto> getCommoditiesByTypeAndStatus(String typeName, String statusName);

    int updateCommodity(int cid, int statusId);

    List<CommodityDto> getCommodityListByUserId(int uid);

    String download(int cid, HttpServletResponse response);

    List<CommodityDto> searchCommodities(String param);

    int getSize(int statusId);
}
