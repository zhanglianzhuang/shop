package com.shop.api.service;

import com.shop.api.pojo.CommodityDto;

import java.util.List;

public interface ShoppingCarService {
    int addCar(int uid, int cid);

    List<CommodityDto> getCar(int uid);

    int removeCommodity(int uid, int cid);

    int reduceCommodity(int uid, int cid, int num);

    int removeAllCommodities(int uid);
}
