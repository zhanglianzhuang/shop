package com.shop.api.service;

import com.shop.persist.entity.CommodityType;

import java.util.List;

public interface CommodityTypeService {

    List<CommodityType> getAllTypes();

    CommodityType getCommodityTypeByTypeId(int typeId);

    CommodityType getCommodityTypeByTypeName(String typeName);
}
