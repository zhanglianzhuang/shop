package com.shop.api.controller;

import com.shop.api.pojo.BaseResponse;
import com.shop.api.pojo.CommodityDto;
import com.shop.api.pojo.RelationDto;
import com.shop.api.service.CommodityFavouriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/favor")
public class CommodityFavouriteController {
    @Autowired
    private CommodityFavouriteService commodityFavouriteService;

    @PostMapping(value = "/add")
    public BaseResponse<Integer> addFavor(@RequestBody RelationDto relationDto) {
        int result = commodityFavouriteService.addFavourite(relationDto.getUid(), relationDto.getCid());
        return BaseResponse.success(result);
    }

    @GetMapping("/getFavors")
    public BaseResponse<List<CommodityDto>> getFavors(@RequestParam(value = "uid") int uid) {
        List<CommodityDto> favourite = commodityFavouriteService.getFavourite(uid);
        return BaseResponse.success(favourite);
    }

    @GetMapping("/deleteFavors")
    public BaseResponse<Integer> removeFavors(@RequestParam(value = "uid") int uid,
                                              @RequestParam(value = "cid") int cid) {
        int result = commodityFavouriteService.removeFavourite(uid, cid);
        return BaseResponse.success(result);
    }
}
