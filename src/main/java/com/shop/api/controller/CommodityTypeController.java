package com.shop.api.controller;

import com.shop.api.service.CommodityTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/type")
public class CommodityTypeController {
    @Autowired
    private CommodityTypeService commodityTypeService;
}
