package com.shop.api.controller;

import cn.hutool.core.util.PhoneUtil;
import com.shop.api.entity.Authentication;
import com.shop.api.pojo.BaseResponse;
import com.shop.api.pojo.CommodityDto;
import com.shop.api.pojo.UserDto;
import com.shop.api.service.UserBuyingService;
import com.shop.api.service.UserService;
import com.shop.core.enume.ResponseEnum;
import com.shop.persist.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/user")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserBuyingService userBuyingService;

    @PostMapping(value = "/login")
    public BaseResponse<UserDto> login(@RequestBody Authentication authentication) {
        if (!PhoneUtil.isMobile(authentication.getPhoneNumber())) {
            return new BaseResponse<>(ResponseEnum.PHONE_NUMBER_INVALID.getCode(), ResponseEnum.PHONE_NUMBER_INVALID.getMessage());
        }
        Optional<UserDto> userCheck = userService.getUserCheck(authentication);
        return userCheck.map(userDto -> new BaseResponse<>(ResponseEnum.SUCCESS.getCode(), ResponseEnum.SUCCESS.getMessage(), userDto))
                .orElseGet(() -> new BaseResponse<>(ResponseEnum.FAILED.getCode(), ResponseEnum.FAILED.getMessage()));
    }

    @PostMapping(value = "/register")
    public BaseResponse<Integer> register(@RequestBody Authentication authentication) {
        if (!PhoneUtil.isMobile(authentication.getPhoneNumber())) {
            return new BaseResponse<>(ResponseEnum.PHONE_NUMBER_INVALID.getCode(), ResponseEnum.PHONE_NUMBER_INVALID.getMessage());
        }
        int result = userService.register(authentication);
        if (result != 1) {
            return BaseResponse.failed();
        }
        return BaseResponse.success(result);
    }

    @PostMapping(value = "/info")
    public BaseResponse<UserDto> getInfo(@RequestParam int uid) {
        User user = userService.getUser(uid);
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(user, userDto);
        return BaseResponse.success(userDto);
    }

    @PostMapping(value = "/changeRole")
    public BaseResponse<Integer> changeRole(@RequestParam int uid, @RequestParam int roleId, @RequestParam String aliPayId) {
        int result = userService.updateUser(uid, roleId, aliPayId);
        return BaseResponse.success(result);
    }

    @GetMapping(value = "/getBuying")
    public BaseResponse<List<CommodityDto>> getBuying(@RequestParam int uid) {
        return BaseResponse.success(userBuyingService.getUserBuying(uid));
    }

    @PostMapping(value = "/changePwd")
    public BaseResponse<Integer> changePassword(@RequestBody Authentication authentication) {
        int result = userService.updatePassword(authentication);
        return BaseResponse.success(result);
    }

    @PostMapping(value = "/changeInfo")
    public BaseResponse<Integer> changeInfo(@RequestBody UserDto userDto) {
        int result = userService.updateInfo(userDto);
        return BaseResponse.success(result);
    }

    @PostMapping(value = "/logout")
    public BaseResponse<Integer> logout(@RequestParam int id) {
        int result = userService.logout(id);
        return BaseResponse.success(result);
    }

    @GetMapping(value = "getAllUser")
    public BaseResponse<List<UserDto>> getAllUser() {
        List<UserDto> result = userService.getUserList();
        return BaseResponse.success(result);
    }

    @GetMapping(value = "getAdmin")
    public BaseResponse<List<UserDto>> getAdminList() {
        List<UserDto> result = userService.getAdminList();
        return BaseResponse.success(result);
    }
}
