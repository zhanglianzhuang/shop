package com.shop.api.controller;

import com.shop.api.pojo.BaseResponse;
import com.shop.api.service.DirectDebitLogService;
import com.shop.persist.entity.DirectDebitLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: zhang
 * @date: 2020/11/8 15:49
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/vi/log")
public class DirectDebitLogController {
    @Autowired
    private DirectDebitLogService directDebitLogService;

    @PostMapping(value = "/getLogs")
    public BaseResponse<List<DirectDebitLog>> getLogs(@RequestParam int uid) {
        List<DirectDebitLog> directDebitLog = directDebitLogService.getDirectDebitLogs(uid);
        return BaseResponse.success(directDebitLog);
    }

    @PostMapping(value = "/getLog")
    public BaseResponse<DirectDebitLog> getLog(@RequestParam String orderId) {
        DirectDebitLog directDebitLog = directDebitLogService.getDirectDebitLog(orderId);
        return BaseResponse.success(directDebitLog);
    }
}
