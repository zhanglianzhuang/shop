package com.shop.api.controller;

import com.shop.api.pojo.BaseResponse;
import com.shop.api.service.MembershipService;
import com.shop.persist.entity.Membership;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: zhang
 * @date: 2020/11/23 20:38
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/mem")
public class MembershipController {
    @Autowired
    private MembershipService membershipService;

    @GetMapping(value = "/getCounts")
    public BaseResponse<Integer> getCounts(@RequestParam(value = "id") int id) {
        Membership membership = membershipService.getMembership(id);
        return BaseResponse.success(membership.getCounts());
    }
}
