package com.shop.api.controller;

import com.shop.api.pojo.BaseResponse;
import com.shop.api.pojo.MessageDto;
import com.shop.common.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/message")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @GetMapping(value = "/getMessage")
    public BaseResponse<List<MessageDto>> getMessage(@RequestParam(value = "uid") int uid) {
        List<MessageDto> message = messageService.getMessage(uid);
        return BaseResponse.success(message);
    }

    @GetMapping(value = "/getUnReading")
    public BaseResponse<Boolean> getUnReadingMessage(@RequestParam(value = "uid") int uid) {
        boolean result = messageService.getUnReading(uid);
        return BaseResponse.success(result);
    }
}
