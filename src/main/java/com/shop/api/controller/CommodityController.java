package com.shop.api.controller;

import com.shop.api.pojo.BaseResponse;
import com.shop.api.pojo.CommodityDto;
import com.shop.api.pojo.UploadDto;
import com.shop.api.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/shop")
public class CommodityController {
    @Autowired
    private CommodityService commodityService;

    @GetMapping(value = "/getAllCommodity")
    public BaseResponse<List<CommodityDto>> getCommodityList(@RequestParam(value = "pageNumber") int pageNumber,
                                                             @RequestParam(value = "pageSize") int pageSize,
                                                             @RequestParam(value = "statusId") int statusId) {
        List<CommodityDto> commodityList = commodityService.getCommodityList((pageNumber - 1) * pageSize, pageSize, statusId);
        return BaseResponse.success(commodityList);
    }

    @GetMapping(value = "/getCommodity")
    public BaseResponse<CommodityDto> getCommodity(@RequestParam(value = "cid") int cid) {
        CommodityDto commodity = commodityService.getCommodity(cid);
        return BaseResponse.success(commodity);
    }

    @GetMapping(value = "/getCommodity/type")
    public BaseResponse<List<CommodityDto>> getCommodityByTypeAndStatus(@RequestParam(value = "typeName") String typeName,
                                                                        @RequestParam(value = "statusName") String statusName) {
        List<CommodityDto> commodities = commodityService.getCommoditiesByTypeAndStatus(typeName, statusName);
        return BaseResponse.success(commodities);
    }

    @PostMapping(value = "/changeStatus")
    public BaseResponse<Integer> changeStatus(@RequestParam(value = "cid") int cid,
                                              @RequestParam(value = "statusId") int statusId) {
        int result = commodityService.updateCommodity(cid, statusId);
        return BaseResponse.success(result);
    }

    @GetMapping(value = "/getByCreator")
    public BaseResponse<List<CommodityDto>> getByCreator(@RequestParam(value = "uid") int uid) {
        List<CommodityDto> commodityListByUserId = commodityService.getCommodityListByUserId(uid);
        return BaseResponse.success(commodityListByUserId);
    }

    @PostMapping(value = "/upload")
    public BaseResponse<String> upload(UploadDto uploadDto) {
        if (uploadDto.getFile().length == 0) {
            return BaseResponse.failed("文件上传失败");
        }
        int result = commodityService.addCommodity(uploadDto);
        return BaseResponse.success(String.valueOf(result));
    }

    @GetMapping(value = "/download")
    public String download(@RequestParam(value = "cid") int cid, HttpServletResponse response) {
        return commodityService.download(cid, response);
    }

    @GetMapping(value = "/search")
    public BaseResponse<List<CommodityDto>> search(@RequestParam(value = "param") String param) {
        List<CommodityDto> commodityDto = commodityService.searchCommodities(param);
        return BaseResponse.success(commodityDto);
    }

    @GetMapping(value = "/getSize")
    public BaseResponse<Integer> getSize(@RequestParam(value = "statusId") int statusId) {
        int size = commodityService.getSize(statusId);
        return BaseResponse.success(size);
    }
}
