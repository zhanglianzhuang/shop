package com.shop.api.controller;

import com.shop.api.pojo.BaseResponse;
import com.shop.api.pojo.CommodityDto;
import com.shop.api.pojo.RelationDto;
import com.shop.api.service.ShoppingCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/car")
public class ShoppingCarController {
    @Autowired
    private ShoppingCarService shoppingCarService;

    @PostMapping(value = "/add")
    public BaseResponse<Integer> addCar(@RequestBody RelationDto relationDto) {
        int result = shoppingCarService.addCar(relationDto.getUid(), relationDto.getCid());
        return BaseResponse.success(result);
    }

    @GetMapping("/getCars")
    public BaseResponse<List<CommodityDto>> getCars(@RequestParam(value = "uid") int uid) {
        List<CommodityDto> car = shoppingCarService.getCar(uid);
        return BaseResponse.success(car);
    }

    @GetMapping("/removeCar")
    public BaseResponse<Integer> removeCars(@RequestParam(value = "uid") int uid,
                                            @RequestParam(value = "cid") int cid) {
        int result = shoppingCarService.removeCommodity(uid, cid);
        return BaseResponse.success(result);
    }

    @GetMapping("/reduceCommodity")
    public BaseResponse<Integer> reduceCommodity(@RequestParam(value = "uid") int uid,
                                                 @RequestParam(value = "cid") int cid,
                                                 @RequestParam(value = "num") int num) {
        int result = shoppingCarService.reduceCommodity(uid, cid, num);
        return BaseResponse.success(result);
    }

    @GetMapping("/removeAllCars")
    public BaseResponse<Integer> removeAllCommodities(@RequestParam(value = "uid") int uid) {
        int result = shoppingCarService.removeAllCommodities(uid);
        return BaseResponse.success(result);
    }
}
