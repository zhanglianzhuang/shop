package com.shop.api.controller;

import com.shop.api.pojo.BaseResponse;
import com.shop.api.pojo.CommodityDto;
import com.shop.api.service.*;
import com.shop.api.service.Impl.PayService;
import com.shop.job.RevenueShareTask;
import com.shop.persist.entity.DirectDebitLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zhang
 * @Date: 2020/11/7 19:27
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/pay")
@Slf4j
public class PayController {
    @Autowired
    private PayService payService;
    @Autowired
    private DirectDebitLogService directDebitLogService;
    @Autowired
    private MembershipService membershipService;
    @Autowired
    private UserBuyingService userBuyingService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommodityService commodityService;
    @Autowired
    RevenueShareTask revenueShareTask;

    /**
     * 下单支付
     */
    @PostMapping(value = "/confirm")
    public BaseResponse<String> pay(@RequestParam int uid, @RequestParam String cid, @RequestParam boolean clear) {
        String result = payService.pay(uid, cid, clear);
        return BaseResponse.success(result);
    }

    @PostMapping(value = "/confirmVIP")
    public BaseResponse<String> payVIP(@RequestParam int uid) {
        String result = payService.payVIP(uid);
        return BaseResponse.success(result);
    }

    @PostMapping(value = "/VIPBuy")
    public BaseResponse<String> buy(@RequestParam int uid, @RequestParam int cid) {
        int counts = membershipService.getMembership(uid).getCounts();
        if (counts > 0) {
            userBuyingService.addUserBuying(uid, cid);
            membershipService.updateMembership(uid, --counts);
            CommodityDto commodity = commodityService.getCommodity(cid);
            userService.updateMemberIncome(uid, commodity.getPrice());
            return BaseResponse.success();
        }
        return BaseResponse.failed();
    }

    /**
     * 回调
     */
    @PostMapping(value = "/callback")
    public void fallback(HttpServletRequest request) {
        log.info(request.toString());
        Map<String, String[]> parameterMap = request.getParameterMap();
        Map<String, String> params = new HashMap<>(32);
        for (String name : parameterMap.keySet()) {
            params.put(name, request.getParameter(name));
        }
        if (params.get("out_trade_no").contains("VIP")) {
            if ("TRADE_SUCCESS".equals(params.get("trade_status"))) {
                String uid = params.get("out_trade_no").split("/")[1];
                membershipService.addMembership(Integer.parseInt(uid), 5);
                userService.updateVIP(Integer.parseInt(uid));
                log.info("{}成为VIP", uid);
                return;
            }
        }

        if ("TRADE_SUCCESS".equals(params.get("trade_status"))) {
            String[] out_trade_nos = params.get("out_trade_no").split("-");
            userBuyingService.addUserBuying(Integer.parseInt(out_trade_nos[0]), Integer.parseInt(out_trade_nos[2]));
        }
        DirectDebitLog directDebitLog = new DirectDebitLog();
        directDebitLog.setPhases(params.get("trade_status"));
        directDebitLog.setOrderId(params.get("out_trade_no"));
        directDebitLog.setAliPayId(params.get("trade_no"));
        directDebitLog.setBuyerId(params.get("buyer_id"));
        directDebitLog.setPrice(Double.parseDouble(params.get("total_amount")));
        directDebitLog.setUDate(LocalDateTime.now());
        log.info("orderId:{} 交易状态更新", params.get("out_trade_no"));
        int i = directDebitLogService.updateDirectDebitLog(directDebitLog);
        log.info("更新结果{}", i);
    }

    @PostMapping(value = "/test")
    public void test() {
        revenueShareTask.share();
    }
}
