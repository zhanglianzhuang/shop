package com.shop.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * SysController
 *
 * @author zhang
 * @version 1.0
 * 2021/3/6 21:45
 */
@CrossOrigin
@Controller
@RequestMapping(value = "/api/v1/sys")
public class SysController {

    @GetMapping(value = "/404")
    public String unFound() {
        return "/404.html";
    }
}
