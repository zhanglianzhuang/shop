package com.shop.api.pojo;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommodityDto {
    private int id;
    private String commodityName;
    private double price;
    private int typeId;
    private int statusId;
    private LocalDateTime CDate;
    private LocalDateTime UDate;
    private String editor;
    private String creator;
    private int creatorId;
    private String introduction;
    private String title;
    private String pic;
}
