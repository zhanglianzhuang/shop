package com.shop.api.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RelationDto {
    private int uid;
    private int cid;
}
