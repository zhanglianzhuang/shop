package com.shop.api.pojo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

/**
 * @author: zhang
 * @date: 2020/11/11 21:13
 */
@Getter
@Setter
public class UploadDto {
    private String commodityName;
    private String title;
    private int creator;
    private double price;
    private String introduction;
    private MultipartFile[] file;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime CDate;
}
