package com.shop.api.pojo;

import com.shop.core.exception.BaseException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: zhang
 * @date: 2020/11/12 20:32
 */
@Getter
@Setter
@AllArgsConstructor
public class ErrorResponse {
    private String cause;
    private String message;
    private String path;

    public ErrorResponse() {
    }

    public ErrorResponse(BaseException ex, String path) {
        this(ex.getCause().toString(), ex.getMessage(), path);
    }

    // 省略 getter/setter 方法

    @Override
    public String toString() {
        return "ErrorReponse{" +
                ", cause=" + cause +
                ", message='" + message + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
