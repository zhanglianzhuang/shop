package com.shop.api.pojo;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MessageDto {
    private int fromUid;
    private int toUid;
    private String content;
    private LocalDateTime CDATE;
    private boolean reading;
    private String fromName;
    private String toName;
}
