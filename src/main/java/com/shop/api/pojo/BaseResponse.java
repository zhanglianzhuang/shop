package com.shop.api.pojo;

import com.shop.core.enume.ResponseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BaseResponse<T> {
    private int code;
    private String message;
    private T data;

    public BaseResponse(int code, String message) {
        this.setCode(code);
        this.setMessage(message);
    }

    public static <T> BaseResponse<T> success() {
        return new BaseResponse<>(ResponseEnum.SUCCESS.getCode(), ResponseEnum.SUCCESS.getMessage());
    }

    public static <T> BaseResponse<T> failed() {
        return new BaseResponse<>(ResponseEnum.FAILED.getCode(), ResponseEnum.FAILED.getMessage());
    }

    public static BaseResponse<Integer> success(int resultCode) {
        return new BaseResponse<>(ResponseEnum.SUCCESS.getCode(), ResponseEnum.SUCCESS.getMessage(), resultCode);
    }

    public static <T> BaseResponse<T> success(T t) {
        return new BaseResponse<>(ResponseEnum.SUCCESS.getCode(), ResponseEnum.SUCCESS.getMessage(), t);
    }

    public static <T> BaseResponse<T> failed(T t) {
        return new BaseResponse<>(ResponseEnum.FAILED.getCode(), ResponseEnum.FAILED.getMessage(), t);
    }
}
