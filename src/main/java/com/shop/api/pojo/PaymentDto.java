package com.shop.api.pojo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author: zhang
 * @date: 2020/11/7 19:43
 */
@Getter
@Setter
public class PaymentDto {
    private String commodityName;
    private BigDecimal price;
}
