package com.shop.api.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Authentication {
    private String phoneNumber;
    private String password;
}
