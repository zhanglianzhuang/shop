package com.shop.api.server;


import com.shop.api.pojo.MessageDto;
import com.shop.common.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/getChat/{uid}")
@Component
@Slf4j
public class WebSocketServer {

    private static ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public void setMyServiceImpl(ApplicationEventPublisher applicationEventPublisher) {
        WebSocketServer.applicationEventPublisher = applicationEventPublisher;
    }

    private static Map<String, Session> sessionMap = new ConcurrentHashMap<>();


    //建立连接对象
    @OnOpen
    public void onOpen(@PathParam("uid") String uid, Session session) {
        sessionMap.put(uid, session);
        log.info("uid:{}连接成功", uid);
    }

    //收到调用的信息
    @OnMessage
    public void onMessage(String message) {
        try {
            Optional<MessageDto> messageDtoOptional = JsonUtil.parse(message, MessageDto.class);
            if (!messageDtoOptional.isPresent()) {
                log.warn("message format error");
                return;
            }
            MessageDto messageDto = messageDtoOptional.get();
            if (sessionMap.containsKey(String.valueOf(messageDto.getToUid()))) {
                sessionMap.get(String.valueOf(messageDto.getToUid())).getAsyncRemote().sendText(message);
                messageDto.setReading(true);
            }
            applicationEventPublisher.publishEvent(messageDto);
        } catch (Exception e) {
            log.error("推送消息失败{}", e.getMessage());
        }
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        log.error("发生位置错误:{},error:{}", session, throwable.getMessage());

    }

    @OnClose
    public void onClose(@PathParam("uid") String uid) {
        sessionMap.remove(uid);
    }
}
